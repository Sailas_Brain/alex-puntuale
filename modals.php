<div class="modal modal-add-recipient">
	<div class="modal__inner modal-add-recipient__inner">
		<div class="col">
			<span class="modal-add-recipient__title">Добавить получателя</span>
				<div class="modal-add-recipient__internal internal-blocks">
					<div class="internal-blocks__initials">
							<input type="text" name="surname" placeholder="Фамилия">
							<input type="text" name="name" placeholder="Имя">
							<input type="text" name="patronumic" placeholder="Отчество">						
					</div>
					<div class="internal-blocks__block-phone">
						<select name="" id="">
							<option value="+7">+7</option>
							<option value="+895">+895</option>
							<option value="8800">8800</option>
						</select>
						<input type="text" name="phone" >
					</div>
					<div class="internal-blocks__button">
						<button type="submit" class="btn btn-submit">ОТМЕНИТЬ</button>
						<button type="submit" class="btn btn-submit">СОХРАНИТЬ</button>
					</div>
				</div>
				<button class="btn btn-modal-close">Закрыть окно</button>
		</div>
	</div>
</div>
<div class="modal modal-paragraphs-pickup">
	<div class="modal__inner modal-paragraphs-pickup__inner">
		<div class="col">
			<div class="modal-paragraphs-pickup__internal internal-blocks">
				
				<div class="internal-blocks__left left-block">
					<span class="modal-add-recipient__title">Добавление пункта самовывоза</span>
					<div class="left-block__paragraph-block">
						<select name="" id="">
							<option value="Павловский Посад">Павловский Посад</option>
							<option value="Павловский Посад">Павловский Посад</option>
							<option value="Павловский Посад">Павловский Посад</option>
						</select>
						<p>Всего пунктов: 1</p>
						<div class="pavlovsky-posad">
							<label for="south">
								<input type="radio" value="" id="south" name="button">    
								<span></span>г. Павловский Посад (Московская область), Южная улица, д. 16А</label>
							</label>
						</div>
						<div class="substantive-item">
							<label for="substantive">
								<input id="substantive" type="checkbox" name="substantive">
								<span></span>Основной пункт самовывоза
							</label>
						</div>
						<div class="button-block">
							<button type="submid">ОТМЕНИТЬ</button>
							<button type="submid">ДОБАВИТЬ</button>
						</div>
					</div>
				</div>
				<div class="internal-blocks__right"></div>
			</div>
			<button class="btn btn-modal-close">Закрыть окно</button>
		</div>
	</div>
</div>
<div class="modal modal-attebtion">
	<div class="modal__inner modal-attebtion__inner">
		<div class="col">
			<div class="modal-attebtion__internal">
				<span>Внимание!<br>
					Выберите доступную дату доставки из календаря.<br>
					Получатель не указан!</span>
	
			</div>
			<button class="btn btn-modal-close">Закрыть окно</button>
		</div>
	</div>
</div>
<div class="modal modal-edit-personal-data">
    <div class="modal__inner modal-edit-personal-data__inner">
        <div class="col">
            <span class="modal-edit-personal-data__title">Мои параметры фигуры</span>
                <div class="modal-edit-personal-data__internal-block options-block">
                    <div class="options-block__left">
                        <p>Для изменения ФИО Вам необходимо написать заявление от 
                        руки в свободной форме с просьбой изменить данные.
                        В заявлении необходимо обязательно указать причину смены
                        данных и номер мобильного телефона, указанный в ЛК. 
                        Заявление можно отправить Заказным письмом на юридический адрес:
                        142715, Московская область, Ленинский район, деревня Мильково,
                        владение 1 — или передать его на ближайший пункт самовывоза.</p>
                    </div>
                        <div class="options-block__right">
                            <p>Чтобы получать подарки и сюрпризы, пожалуйста, укажите свои данные.</p>
                            <div class="select-data-birth">
                                <div class="text-data">
                                <span>Дата рождения</span>
                                </div>
                                    <select name="" id="">
                                        <option value="01">01</option>
                                        <option value="02">02</option>
                                        <option value="03">03</option>
                                        <option value="04">04</option>
                                        <option value="05">05</option>
                                        <option value="06">06</option>                                    
                                    </select>
                                    <select name="" id="">
                                        <option value="Январь">Январь</option>
                                        <option value="Февраль">Февраль</option>
                                        <option value="Март">Март</option>
                                        <option value="Апрель">Апрель</option>
                                        <option value="Май">Май</option>
                                        <option value="Июнь">Июнь</option>                                    
                                    </select>
                                    <select name="" id="">
                                        <option value="">1990</option>
                                        <option value="">1991</option>
                                        <option value="">1992</option>
                                        <option value="">1993</option>
                                        <option value="">1994</option>
                                        <option value="">1995</option>                                    
                                    </select>
                                    <i class="fas fa-times"></i>
                            </div>
                            <div class="visibility">
                                <div class="text-data">
                                    <span>Видимость</span>
                                </div>
                                <select name="" id="">
                                        <option value="Показывать дату рождения">Показывать дату рождения</option>
                                        <option value="Показывать дату рождения">Скрыть дату рождения</option>
                                        <option value="Показывать дату рождения">Показывать дату рождения</option>                               
                                    </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-edit-personal-data__warning">
                        <div class="icons">
                            <img src="/images/my-data/exclamation.png" alt="">
                        </div>
                            <div class="text">
                                <p>В случае, если на Личном Счете Клиента есть денежные средства, 
                                перед изменением данных представители интернет-магазина оставляют
                                за собой право потребовать документ, удостоверяющий личность.</p>
                            </div>
                    </div>
                    <div class="modal-edit-personal-data__action-button">
                        <button type="submit" class="btn btn-submit">Отменить</button>
                        <button type="submit" class="btn btn-submit">Сохранить</button>
                    </div> 
                    <button class="btn btn-modal-close">Закрыть окно</button>
        </div>
    </div>
</div>
<div class="modal modal-addition-protection">
    <div class="modal__inner modal-addition-protection__inner">
        <div class="col">
            <span class="modal-addition-protection__title">Дополнительная защита для входа</span>
            <div class="modal-addition-protection__internal internal-block">
                <div class="internal-block__top">
                    <p>При включенной функции, для входа на сайт,
                    помимо логина и пароля, потребуется дополнительно
                    ввести код, который будет приходить Вам по СМС.</p>
                </div>
                <div class="internal-block__center">
                    <p>Это значительно увеличит безопасность Вашего аккаунта.</p>
                        <div class="radio-button">
                            
                            <label for="enadled">
                            <input type="radio" value="Включено" id="enadled" name="button" checked>
                            <span></span>Включено</label>

                            
                            <label for="tarned-off">
                            <input type="radio" value="Выключено" id="tarned-off" name="button">    
                            <span></span>Выключено</label>

                        </div>
                </div>
                <div class="internal-block__bottom">
                    <button type="submit" class="btn btn-submit">ОТМЕНИТЬ</button>
                    <button type="submit" class="btn btn-submit">ПРИМЕНИТЬ</button>
                </div>
            </div>
            <button class="btn btn-modal-close">Закрыть окно</button>
        </div>
    </div>
</div>
<div class="modal modal-change-email">
    <div class="modal__inner modal-change-email__inner">
        <div class="col">
            <span class="modal-change-email__title">Изменение электронной почты</span>
            <div class="modal-change-email__internal internal-blocks">
                <div class="internal-blocks__top">
                    <p>Адрес электронной почты *</p>
                    <input type="text" name="email" placeholder="Введите ваш email">
                </div>
                <div class="internal-blocks__center">
                    <p>На указанный Вами электронный адрес будет выслано письмо с ссылкой, перейдите<br> по ней для подтверждения адреса электронной почты.</p>
                    <p>Подписки на рассылки, выбранные Вами ранее, сохраняются при изменении <br> электронного адреса.</p>
                </div>
                <div class="internal-blocks__button">
                    <button type="submit" class="btn btn-submit">ОТМЕНИТЬ</button>
                    <button type="submit" class="btn btn-submit">ВЫСЛАТЬ КОД ПОДТВЕРЖДЕНИЯ</button>
                </div>
            </div>
            <button class="btn btn-modal-close">Закрыть окно</button>
        </div>
    </div>
</div>
<div class="modal modal-change-phone">
    <div class="modal__inner modal-change-phone__inner">
        <div class="col">
            <span class="modal-change-phone__title">Изменение номера телефона</span>
            <div class="modal-change-phone__internal internal-blocks">
                <div class="internal-blocks__top">
                    <p>Укажите новый номер</p>
                    <div class="select-phone">
                        <select name="" id="">
                            <option value="+7">+7</option>
                            <option value="+56">+56</option>
                            <option value="+29">+29</option>
                            <option value="+67">+67</option>
                        </select>
                        <input type="text" name="tell" placeholder="(965) 242-29-99">
                    </div>
                </div>
                <div class="internal-blocks__buttom">
                    <p>Изменить номер мобильного телефона можно не более 2-х раз в сутки. 
                    В случае неоднократного подтверждения одного и того же номера в разных
                    Личных кабинетах, номер заблокируется.<br> При наличии денежных средств на
                    Личном счете Клиента,<br> интернет-магазин оставляет за собой право
                    потребовать<br> документ, удостоверяющий личность.</p>
                </div>
            </div>
        </div>
        <button class="btn btn-modal-close">Закрыть окно</button>
    </div>
</div>
<div class="modal modal-change-password">
    <div class="modal__inner modal-change-password__inner">
        <div class="col">
            <span class="modal-change-password__title">Изменение пароля</span>
                <div class="modal-change-password__internal internal-block">
                    <div class="internal-block__password">
                        <p>Текущий пароль*</p>
                        <input type="text" name="" placeholder="Введите текущий пароль">
                        <p>Новый пароль*</p>
                        <input type="text" name="" placeholder="Введите новый пароль">
                        <p>Подтверждение пароля*</p>
                        <input type="text" name="" placeholder="Введите подтверждение пароля">
                    </div>
                    <div class="internal-block__button">
                    <button type="submit" class="btn btn-sabmit">ОТМЕНИТЬ</button>
                    <button type="submit" class="btn btn-sabmit">СОХРАНИТЬ</button>
                    </div>
                </div>
            <button class="btn btn-modal-close">Закрыть окно</button>
        </div>
    </div>
</div>
<div class="modal modal-my-shape-options">
    <div class="modal__inner modal-my-shape-options__inner">
        <div class="col">
            <span class="modal-my-shape-options__title">Изменение пароля</span>
                <div class="modal-my-shape-options__internal internal-block">
                    <div class="internal-block__left-block">
                        <p>Рост (см)</p>
                        <p>Вес (кг)</p>
                        <p>Воротник (см)</sppan>
                        <p>Плечи (см)</p>
                        <p>Живот (см)</p>
                        <p>Бицепс (см)</p>
                        <p>Длина рукова (см)</p>
                        <p>Грудь (см)</p>
                        <p>Бедра (см)</p>
                        <p>Талия (см)</p>
                        <p>Обхват запястья (см)</p>
                        <p>Длина рубашки (см)</p>
                    </div>
                    <div class="internal-block__center-block">
                        <form action="" method="">
                            <div class="block-info">
                                <input type="text" name="" placeholder="см">
                                <img src="/images/my-data/line.png" alt="">
                                <select name="" id="">
                                    <option value="Выбрать">Выбрать</option>
                                    <option value="22">22</option>           
                                    <option value="23">23</option>
                                </select>
                            </div>
                            <div class="block-info">
                                <input type="text" name="" placeholder="кг">
                                <img src="/images/my-data/line.png" alt="">
                                <select name="" id="">
                                    <option value="Выбрать">Выбрать</option>
                                    <option value="22">50</option>
                                    <option value="23">60</option>
                                </select>
                            </div>
                            <? for ($j = 0;$j <= 10; $j++) {?>
                                <div class="block-info">
                                    <input type="text" name="" placeholder="см">
                                    <img src="/images/my-data/line.png" alt="">
                                    <select name="" id="">
                                        <option value="Выбрать">Выбрать</option>                                    
                                        <option value="22">22</option>
                                        <option value="23">23</option>
                                    </select>
                                </div>
                            <? } ?>
                        </form>
                    </div>
                    <div class="internal-block__right-block">
                        <img src="/images/my-data/telo.png" alt="">
                    </div>
                </div>
                <button class="btn btn-modal-close">Закрыть окно</button>
        </div>
    </div>
</div>
<div class="modal modal-calculate-delivery">
    <div class="modal_inner modal-calculate-delivery__inner">
        <div class="col-1">
            <h2 class="modal-calculate-delivery__header">Как рассчитать общее кол-во товаров, которое можно оформить на доставку без предварительной оплаты</h2>
            <div class="description-block">
                <div class="description-block__col-1">
                    <p>Данное количество товаров зависит от вашего процента выкупа и суммы выкупа.</p>
                    <p><span>Процент выкупа</span> — отношение стоимости и выкупленного товара к стоимости заказанного товара в процентах, рассчитанное за последние полгода сотрудничества с нами</p>
                    <p><span>Сумма выкупа</span> — сумма, на которую вы совершили покупки в нашем магазине с момента первого заказа.</p>
                </div>
                <div class="description-block__col-2">
                    <b class="description-block__value">29 553 руб.</b>
                    <p class="description-block__name">Ваша сумма выкупа</p>
                    <b class="description-block__value">22,25%</b>
                    <p class="description-block__name">Ваш процент выкупа</p>
                </div>
            </div>
            <p class="limit-coverage">Ограничение на количество товаров в доставке действует только при наличном расчете.</p>
            <p class="par-heavy">При безналичном расчете нет ограничения на количество тооваров, которое можно оформить на доставку.</p>
            <table class="table">
                <tr>
                    <th>Сумма выкупа (руб.)</th>
                    <th>Бронза <span>от 0</span></th>
                    <th>Серебро <span>от 3 000</span></th>
                    <th>Золото <span>от 20 000</span></th>
                    <th>VIP <span>от 100 000</span></th>
                </tr>
                <tr>
                    <td class="table__sub-header-1">% выкупа</td>
                    <td class="table__sub-header-2" colspan="4">Количепство товаров, которое можно оформить на доставку без предварительной оплаты, шт</td>
                </tr>
                <tr>
                    <td>50—100</td>
                    <td>до 10</td>
                    <td>до 30</td>
                    <td>до 150</td>
                    <td>до 500</td>
                </tr>
                <tr>
                    <td>40—50</td>
                    <td>до 10</td>
                    <td>до 25</td>
                    <td>до 100</td>
                    <td>до 250</td>
                </tr>
                <tr>
                    <td>30—40</td>
                    <td>до 10</td>
                    <td>до 20</td>
                    <td>до 50</td>
                    <td>до 150</td>
                </tr>
                <tr>
                    <td>17—30</td>
                    <td>до 10</td>
                    <td>до 10</td>
                    <td>до 30</td>
                    <td>до 50</td>
                </tr>
                <tr>
                    <td>10—17</td>
                    <td>до 10</td>
                    <td>до 10</td>
                    <td>до 10</td>
                    <td>до 10</td>
                </tr>
                <tr>
                    <td>0—10</td>
                    <td>до 0</td>
                    <td>до 0</td>
                    <td>до 0</td>
                    <td>до 0</td>
                </tr>
            </table>
            <button class="btn btn-modal-close">Закрыть окно</button>
        </div>
    </div>
</div>
<div class="modal modal-calculate-buyout">
    <div class="modal_inner modal-calculate-buyout__inner">
        <div class="col-1">
            <h2 class="modal-calculate-buyout__header">Таблица расчета процента выкупа</h2>
            <div class="table-wrapper">
                <table class="formula-table">
                    <tr>
                        <td><span class="formula-table__underline">Оплачено</span></td>
                        <td class="formula-table__full-height" rowspan="2">x &nbsp; 100% &nbsp; &nbsp; =></td>
                        <td><span class="formula-table__underline--wider">10 754</span></td>
                        <td class="formula-table__full-height" rowspan="2">x &nbsp; 100% &nbsp; &nbsp; = &nbsp; &nbsp;22,25%</td>
                    </tr>
                    <tr>
                        <td>(Возвращено + Оплачено - Брак)</td>
                        <td>(37 574 + 10 754 - 0)</td>
                    </tr>
                </table>
            </div>
            <div class="info-block">
                <div class="info-block__col-1">
                    <p>Расчет за период <span class="interval">с 20.09.2017 по 22.03.2018</span></p>
                    <p>(обновляется каждый день)</p>
                </div>
                <div class="info-block__col-2">
                    <p>Процент выкупа на завтра составит <span class="buyback-value">22,25%</span><i class="questionmark-icon"></i></p>
                    <div class="dropdown-container">
                        <button class="btn btn-show-calculator">Калькулятор расчета процента</button>
                        <div class="calculator dropdown-block">
                            <p class="calculator__title">Введите сумму предполагаемой покупки, возврата и узнайте, как изменится Ваш процент выкупа</p>
                            <div class="calculator__top-cols">
                                <div class="calculator__col-1">
                                    <b>Сумма покупки</b>
                                    <span>1500</span>
                                </div>
                                <div class="calculator__col-2">
                                    <b>Сумма возврата</b>
                                    <span>700</span>
                                </div>
                            </div>
                            <button class="btn btn-calculate">Рассчитать</button>
                            <span class="calculator__result">22,25%</span>
                            <p class="calculator__explanation">Расчет предварительный, произведен на текущий момент на основании введенных данных</p>
                        </div>
                    </div>
                </div>
            </div>
            <section class="orders">
                <ul class="orders__headers">
                    <li>№ заказа</li>
                    <li>Оплачено</li>
                    <li>Возвращено</li>
                    <li>Учет в проценте выкупа</li>
                </ul>
                <div class="order">
                    <div class="order__item">
                        <div class="order__description">
                            <div class="order__order-number">
                                <b>24977654</b>
                            </div>
                            <div class="order__item-picture-frame">
                                <img class="order__item-picture" src="/images/catalog/dynamic/catalog-products-item.png" alt="">
                            </div>
                            <div class="order__description-info">
                                <b class="order__item-name">Рубашка</b>
                                <b class="order__item-code">Артикул: <span>2621383</span></b>
                                <b class="order__item-color">Цвет: <span>белый</span></b>
                                <b class="order__item-size">Размер: <span>38</span></b>
                            </div>
                        </div>
                        <div class="order__paid">
                            <b>1 1468 руб.</b>
                            <b>11.11.2017</b>
                        </div>
                        <div class="order__returned">
                            <b>1 1468 руб.</b>
                            <b>11.11.2017</b>
                        </div>
                        <div class="order__accounting">
                        </div>
                    </div>
                </div>
                <div class="order">
                    <div class="order__item">
                        <div class="order__description">
                            <div class="order__order-number">
                                <b>24977654</b>
                            </div>
                            <div class="order__item-picture-frame">
                                <img class="order__item-picture" src="/images/catalog/dynamic/catalog-products-item.png" alt="">
                            </div>
                            <div class="order__description-info">
                                <b class="order__item-name">Рубашка</b>
                                <b class="order__item-code">Артикул: <span>2621383</span></b>
                                <b class="order__item-color">Цвет: <span>белый</span></b>
                                <b class="order__item-size">Размер: <span>38</span></b>
                            </div>
                        </div>
                        <div class="order__paid">
                            <b>1 1468 руб.</b>
                            <b>11.11.2017</b>
                        </div>
                        <div class="order__returned">
                            <b>1 1468 руб.</b>
                            <b>11.11.2017</b>
                        </div>
                        <div class="order__accounting">
                        </div>
                    </div>
                    <div class="order__item">
                        <div class="order__description">
                            <div class="order__order-number">
                                <b>24977654</b>
                            </div>
                            <div class="order__item-picture-frame">
                                <img class="order__item-picture" src="/images/catalog/dynamic/catalog-products-item.png" alt="">
                            </div>
                            <div class="order__description-info">
                                <b class="order__item-name">Рубашка</b>
                                <b class="order__item-code">Артикул: <span>2621383</span></b>
                                <b class="order__item-color">Цвет: <span>белый</span></b>
                                <b class="order__item-size">Размер: <span>38</span></b>
                            </div>
                        </div>
                        <div class="order__paid">
                            <b>1 1468 руб.</b>
                            <b>11.11.2017</b>
                        </div>
                        <div class="order__returned">
                            <b>1 1468 руб.</b>
                            <b>11.11.2017</b>
                        </div>
                        <div class="order__accounting">
                        </div>
                    </div>
                </div>
                <table class="order__total">
                    <tr>
                        <td>Итого</td>
                        <td>10 754</td>
                        <td>37 574</td>
                    </tr>
                    <tr>
                        <td>Итого (количество)</td>
                        <td>14</td>
                        <td>22</td>
                    </tr>
                </table>
            </section>
            <button class="btn btn-modal-close">Закрыть окно</button>
        </div>
    </div>
</div>
<div class="modal modal-calculate-discount">
    <div class="modal_inner modal-calculate-discount__inner">
        <div class="col">
            <div class="titlebar">
                <span class="popup-title">Как рассчитать скидку постоянного покупателя?</span>
                <a href="#" class="close"></a>
            </div>
            <p class="description">Скидка рассчитывается на основе покупок, сделанных через Личный кабинет, и зависит от суммы выкупа и процента выкупа заказанных товаров за последние полгода. Обратите внимание: на скидку влияет каждая ваша покупка и возврат, пересчет производится моментально. В случае, если Клиент не совершает покупок в течение полугода, процент выкупа рассчитывается за весь период сотрудничества.</p>
            <table class="info-table">
                <tbody>
                    <tr>
                        <td>
                            <p class="red">0 руб.</p>
                            <p>Ваша сумма выкупа</p>
                        </td>
                        <td>
                            <p class="red">0%</p>
                            <p>Ваш процент выкупа</p>
                        </td>
                    </tr>
                </tbody>
            </table>
            <table class="discount-table">
                <thead>
                    <tr>
                        <th scope="row">Суммы выкупа<br>(₽)
                        </th>
                        <th class="discount-type">Бронза <strong>от 10&nbsp;000</strong> <i class="i discount-bronze">&nbsp;</i></th>
                        <th class="discount-type">Серебро <strong>от 50&nbsp;000</strong> <i class="i discount-silver">&nbsp;</i></th>
                        <th class="discount-type">Золото <strong>от 100&nbsp;000</strong> <i class="i discount-gold">&nbsp;</i></th>
                        <th class="discount-type">VIP <strong>от 250&nbsp;000</strong> <i class="i discount-vip">&nbsp;</i></th>
                    </tr>
                    <tr>
                        <th>% выкупа</th>
                        <th colspan="4">Ваша скидка в %</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>20—30</td>
                        <td>0</td>
                        <td>5</td>
                        <td>7</td>
                        <td>10</td>
                    </tr>
                    <tr>
                        <td>30—40</td>
                        <td>5</td>
                        <td>7</td>
                        <td>10</td>
                        <td>15</td>
                    </tr>
                    <tr>
                        <td>40—60</td>
                        <td>7</td>
                        <td>10</td>
                        <td>15</td>
                        <td>17</td>
                    </tr>
                    <tr>
                        <td>60—100</td>
                        <td>10</td>
                        <td>15</td>
                        <td>17</td>
                        <td>17</td>
                    </tr>
                </tbody>
            </table>
            <button class="btn btn-modal-close">Закрыть окно</button>
        </div>
    </div>
</div>
<div class="modal modal-create-appeal">
    <div class="modal_inner modal-create-appeal__inner">
        <div class="col">
            <p class="title">Создание обращения</p>
            <form>
                <input list="create-appeal__theme" name="create-appeal__theme" placeholder="Выберите тему *">
                <datalist id="create-appeal__theme">
                    <option value="Тема 1">
                    <option value="Тема 2">
                    <option value="Тема 3">
                </datalist>
                <input list="create-appeal__section" name="create-appeal__section" placeholder="Выберите раздел *">
                <datalist id="create-appeal__section">
                    <option value="Раздел 1">
                    <option value="Раздел 2">
                    <option value="Раздел 3">
                </datalist>
                <textarea name="questions-form-textarea" cols="90" rows="10" placeholder="Пожалуйста, подробно опишите ваше обращение. Наиболее полное описание позволит нам, предоставить вам ответ, в кратчайшие сроки без уточнения дополнительной информации."></textarea>
                <p class="fields-required">* Обязательные для заполнения поля</p>
                <input type="reset" class="btn btn-submit" value="Отмена">
                <input type="submit" class="btn btn-submit" value="Отправить">
            </form>
            <button class="btn btn-modal-close">Закрыть окно</button>
        </div>
    </div>
</div>
<div class="modal modal-gift-item-details">
    <div class="modal__inner modal-gift-item-details__inner">
        <div class="col">
            <span class="modal-gift-item-details__title">Детализация подарочных средств</span>
                <div class="modal-gift-item-details__internal internal-blocks">
                    <div class="internal-blocks__activated">
                        <input id="radio-button" value="Активированные" type="radio" name="radio-button" checked>
                        <label for="radio-button">Активированные</label>
                        <input id="radio-button-1" value="Ожидают активации" type="radio" name="radio-button">
                        <label for="radio-button-1">Ожидают активации</label>
                        <form action=""> 
                            <input type="search" name="" results="5" placeholder="Найти" autosave="unique">
                            <button><i class="fas fa-search"></i></button> 
                        </form>
                    </div>
                    <div class="internal-blocks__information">
                        <span>Дата активации</span>
                        <span>Тип подарка</span>
                        <span>Заказ</span>
                        <span>Артикул</span>
                        <span>№ подарка/бонуса</span>
                        <span>Сумма</span>
                        <span>Истечение срока действия</span>
                    </div>
                </div>
                <button class="btn btn-modal-close">Закрыть окно</button>
        </div>
    </div>
</div>
<div class="modal modal-your-certificate">
    <div class="modal__inner modal-your-certificate__inner">
        <div class="col">
            <span class="modal-your-certificate__title">ВАШИ СЕРТИФИКАТЫ</span>
            <div class=" modal-your-certificate__internal internal-block">
                <div class="internal-block__information">
                    <span>Дата и время</span>
                    <span>Номер</span>
                    <span>Сумма</span>
                    <span>Статус</span>
                    <span>Ошибка</span>
                </div>
            </div>
            <button class="btn btn-modal-close">Закрыть окно</button>
        </div>
    </div>
</div>
<div class="modal modal-continue-return-request">
    <div class="modal_inner modal-continue-return-request__inner">
        <div class="col">
            <p class="header">Выберите способ оформления возврата</p>
            <div class="return">
                <a class="return__method" href="">
                    <b class="return__name">В любом фирменном пункте Puntuale</b>
                    <p class="return__description">Предварительно оформлять документы на возврат товара в пункте самовывоза не требуется.</p>
                    <b class="return__cost">Бесплатно</b>
                </a>
                <a class="return__method" href="">
                    <b class="return__name">Курьером</b>
                    <p class="return__description">Нажмите для продолжения оформления возврата с помощью курьерской доставки.</p>
                    <b class="return__cost">200 рублей</b>
                </a>
                <a class="return__method" href="">
                    <b class="return__name">Почтой</b>
                    <p class="return__description">Вы можете сделать возврат товаров воспользовавшись услугами почты. Для получения более подробной информации нажмите кнопку</p>
                    <b class="return__cost">Согласно тарифам</b>
                </a>
            </div>
            <button class="btn btn-modal-close">Закрыть окно</button>
        </div>
    </div>
</div>
<div class="modal modal-cancel-order-request">
    <div class="modal_inner modal-cancel-order-request__inner">
        <div class="col">
            <p>Запрос на отмену товаров в доставке <span>786905</span> был отправлен</p>
            <button class="btn btn-modal-close">Закрыть окно</button>
        </div>
    </div>
</div>
<div class="modal modal-inaccurate-description">
    <div class="modal_inner modal-inaccurate-description__inner">
        <div class="col">
            <p class="title">Нашли неточность в карточке товара?</p>
            <p class="sub-title">Сообщите об этом нашим специалистам с помощью специальной формы обратной связи</p>
            <form>
                <select name="innacurate-description-category" placeholder="Выберите категорию">
                    <option value="Категория 1">Категория 1</option>
                    <option value="Категория 2">Категория 2</option>
                    <option value="Категория 3">Категория 3</option>
                </select>
                <textarea name="questions-form-textarea" cols="90" rows="10" placeholder="Напишите ваш вопрос по товару"></textarea>
                <button type="submit" class="btn btn-submit">Отправить</button>
            </form>
            <button class="btn btn-modal-close">Закрыть окно</button>
        </div>
    </div>
</div>
<div class="modal modal-found-cheaper">
    <div class="modal_inner modal-found-cheaper__inner">
        <div class="col">
            <p class="title">Нашли дешевле?</p>
            <p class="sub-title">Отправьте ссылку на данный товар в другом магазине, <br>где цена ниже, чем у нас</p>
            <form>
                <input class="found-cheaper-link" type="text" name="found-cheaper-link" placeholder="Ссылка на товар">
                <div class="modal-found-cheaper__choose-size">
                    <b>Размер</b>
                    <ul>
                        <li><label><input class="item-size-input" type="radio" name="item-size" value="37"><span>37</span></label></li>
                        <li><label><input class="item-size-input" type="radio" name="item-size" value="38"><span>38</span></label></li>
                        <li><label><input class="item-size-input" type="radio" name="item-size" value="39"><span>39</span></label></li>
                        <li><label><input class="item-size-input" type="radio" name="item-size" value="40"><span>40</span></label></li>
                        <li><label><input class="item-size-input" type="radio" name="item-size" value="41"><span>41</span></label></li>
                        <li><label><input class="item-size-input" type="radio" name="item-size" value="42" checked><span>42</span></label></li>
                        <li><label><input class="item-size-input" type="radio" name="item-size" value="43"><span>43</span></label></li>
                        <li><label><input class="item-size-input" type="radio" name="item-size" value="44"><span>44</span></label></li>
                    </ul>
                </div>
                <input class="found-cheaper-price" type="text" name="found-cheaper-price" placeholder="Цена, руб.">
                <p class="sub-title">Мы ответим о результате рассмотрения сообщения <br>на Ваш E-mail</p>
                <div class="terms-of-action">
                    <a href="">Правила и условия акции</a>
                </div>
                <button type="submit" class="btn btn-submit">Отправить ссылку</button>
            </form>
            <button class="btn btn-modal-close">Закрыть окно</button>
        </div>
    </div>
</div>
<div class="modal modal-find-your-size">
    <div class="modal_inner modal-find-your-size__inner">
        <div class="col">
            <p class="title">Определить размер?</p>
            <table> 
                <tr class=""> <th colspan="2">Размер рубашки</th> <?for ($i=0; $i < 12; $i++) {?> <td>1</td> <?}?> </tr>
                <tr> </tr>
                <tr> <th colspan="2">Охват шеи</th> <?for ($i=0; $i < 12; $i++) {?> <td>1</td> <?}?> </tr>
                <tr> <th>Обхват груди</th> <?for ($i=0; $i < 13; $i++) {?> <td>2<br>3<br>4</td> <?}?> </tr>
                <tr> <th>Обхват талии</th> <?for ($i=0; $i < 13; $i++) {?> <td>2<br>3<br>4</td> <?}?> </tr>
                <tr> <th>Длина по спинке</th> <?for ($i=0; $i < 13; $i++) {?> <td>2<br>3<br>4</td> <?}?> </tr>
                <tr> <th>Длина рукава</th> <?for ($i=0; $i < 13; $i++) {?> <td>2<br>3<br>4</td> <?}?> </tr>
            </table>
            <button class="btn btn-modal-close">Закрыть окно</button>
        </div>
    </div>
</div>
<div class="modal modal-rate-delivery-service">
    <div class="modal_inner modal-rate-delivery-service__inner">
        <div class="col">
            <form>
                <p class="title">Определить размер?</p>
                <p class="sub-title">Для улучшения кач-ва сервиса и работы интернет-магазина, пожалуйста оцените обслуживание.</p>
                <p class="category">1. Оцените кач-во работы курьера</p>
                <i class="rating-placeholder"></i>
                <p class="category">2. Ваши замечания</p>
                <ul>
                    <li><label><input type="checkbox"><i class="checkbox-indicator"></i>Не вежливый курьер</label></li>
                    <li><label><input type="checkbox"><i class="checkbox-indicator"></i>Курьер приехал не в указанное время</label></li>
                    <li><label><input type="checkbox"><i class="checkbox-indicator"></i>Курьер не предупредил заранее о доставке заказа</label></li>
                    <li><label><input type="checkbox"><i class="checkbox-indicator"></i>Курьер не предоставил положенное время на примерку (20 мин)</label></li>
                    <li><label><input type="checkbox"><i class="checkbox-indicator"></i>Отсутствие кассового аппарата</label></li>
                    <li><label><input type="checkbox"><i class="checkbox-indicator"></i>Отсутствие терминала безналичной оплаты</label></li>
                </ul>
                <p class="category">3. Ваши комментарии и пожелания по работе курьера интернет-магазина</p>
                <textarea name="questions-form-textarea" cols="90" rows="10" placeholder="Пожалуйста, опишите качество обслуживания."></textarea>
                <div class="buttons-container">
                    <input type="reset" class="btn btn-submit" value="Отмена">
                    <input type="submit" class="btn btn-submit" value="Отправить">
                </div>
            </form>
            <button class="btn btn-modal-close">Закрыть окно</button>
        </div>
    </div>
</div>
<div class="modal modal-recipient">
    <div class="modal__inner modal-recipient__inner">
        <div class="col">
            <span class="modal-recipient__title">Изменение номера телефона</span>
            <div class="modal-recipient__internal internals-blocks">
                <div class="internals-blocks__initials">
                    <span>Фамилия*</span>
                    <input type="text" name="surname" placeholder="Введите Фамилию">
                </div>
                <div class="internals-blocks__initials">
                    <span>Имя*</span>
                    <input type="text" name="name" placeholder="Введите Имя">
                </div>
                <div class="internals-blocks__initials">
                    <span>Отчество*</span>
                    <input type="text" name="patronymic" placeholder="Введите Отчество">
                </div>
                <div class="internals-blocks__number-phone">
                    <div><span>Мобильный телефон*</span></div>
                    <select name="" id="">
                        <option value="+7">+7</option>
                        <option value="+56">8800</option>
                        <option value="+29">+29</option>
                        <option value="+67">+67</option>
                    </select>
                    <input type="text" name="tell" placeholder="(965) 242-29-99">
                </div>
                <div class="internals-blocks__button">
                    <button type="submit" class="btn btn-submit">Отменить</button>
                    <button type="submit" class="btn btn-submit">Сохранить</button>
                </div>
            </div>
        </div>
        <button class="btn btn-modal-close">Закрыть окно</button>
    </div>
</div>
<div class="modal modal-yandex-map">
    <div class="modal__inner modal-yandex-map__inner">
        <div class="col">
        <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A620c057af9aeb3dd5968340d0b931d4865ad11a776b5c85e037c624b1ee45c92&amp;width=100%25&amp;height=860&amp;lang=ru_RU&amp;scroll=true"></script>         
        </div>
        <button class="btn btn-modal-close">Закрыть окно</button>
    </div>
</div>