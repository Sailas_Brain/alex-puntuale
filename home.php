<?
// ini_set('error_reporting', E_ALL);
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Document</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
		<meta charset="UTF-8">
		<!-- vendor -->
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
		<link rel="stylesheet" href="/css/jquery-ui.css" type="text/css" />
		<link rel="stylesheet" href="/css/slick.css" type="text/css" />
		<link rel="stylesheet" href="/css/slick-theme.css" type="text/css" />
		<!-- common -->
		<link rel="stylesheet" href="/css/main.css" type="text/css" />
	</head>
	<body class="body-home">
		<?include('pages/home/header.php');?>

		<?include('pages/home/home.php');?>

		<?include('footer.php');?>

		<?include('modals.php');?>
		
		<script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
		<!-- vendor -->
        <script type="text/javascript" src="js/vendor/jquery.js"></script>
        <script type="text/javascript" src="js/vendor/jquery-ui.js"></script>
        <script type="text/javascript" src="js/vendor/slick.js"></script>
		<!-- common -->
        <script type="text/javascript" src="js/main.js"></script>
		<?
			$dir = "js/pages";
			$files = scandir($dir);   
			foreach($files as $file){  
				if(($file !== '.') AND ($file !== '..')){
					echo '<script type="text/javascript" src="'.$dir.'/'.$file.'"></script>';
				}
			}
		?>
    </body>
</html>