<?
// ini_set('error_reporting', E_ALL);
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Document</title>
		<meta name="viewport" content="width=1220, initial-scale=0, user-scalable=yes">
		<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
		<meta charset="UTF-8">
		<!-- vendor -->
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
		<link rel="stylesheet" href="/css/jquery-ui.css" type="text/css" />
		<link rel="stylesheet" href="/css/slick.css" type="text/css" />
		<link rel="stylesheet" href="/css/slick-theme.css" type="text/css" />
		<!-- common -->
		<link rel="stylesheet" href="/css/main.css" type="text/css" />
	</head>
	<body>
		<?include('header.php');?>
		<?
			switch ($_GET['p']) {
				case 'auth':
					require_once('pages/auth/auth.php');
					break;
				case 'cart':
					require_once('pages/cart/cart.php');
					break;
				case 'catalog':
					require_once('pages/catalog/catalog.php');
					break;
				case 'delivery':
					require_once('pages/delivery/delivery.php');
					break;
				case 'discount':
					require_once('pages/discount/discount.php');
					break;
				case 'links':
					require_once('pages/links/links.php');
					break;
				case 'my-account':
					require_once('pages/my-account/my-account.php');
					break;
				case 'my-appeals':
					require_once('pages/my-appeals/my-appeals.php');
					break;
				case 'my-balance':
					require_once('pages/my-balance/my-balance.php');
					break;
				case 'my-data':
					require_once('pages/my-data/my-data.php');
					break;
				case 'my-orders':
					require_once('pages/my-orders/my-orders.php');
					break;
				case 'my-wardrobe':
					require_once('pages/my-wardrobe/my-wardrobe.php');
					break;
				case 'payment':
					require_once('pages/payment/payment.php');
					break;
				case 'payment-methods':
					require_once('pages/payment-methods/payment-methods.php');
					break;
				case 'postponed-items':
					require_once('pages/postponed-items/postponed-items.php');
					break;
				case 'product-card':
					require_once('pages/product-card/product-card.php');
					break;
				case 'thanks':
					require_once('pages/thanks/thanks.php');
					break;
				case 'waiting-list':
					require_once('pages/waiting-list/waiting-list.php');
					break;
				default:
					require_once('pages/links/links.php');
			}
		?>

		<?include('footer.php');?>
		<?include('modals.php');?>
		
		<script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
		<!-- vendor -->
        <script type="text/javascript" src="js/vendor/jquery.js"></script>
        <script type="text/javascript" src="js/vendor/jquery-ui.js"></script>
        <script type="text/javascript" src="js/vendor/slick.js"></script>
		<!-- common -->
        <script type="text/javascript" src="js/main.js"></script>
		<?
			$dir = "js/pages";
			$files = scandir($dir);   
			foreach($files as $file){  
				if(($file !== '.') AND ($file !== '..')){
					echo '<script type="text/javascript" src="'.$dir.'/'.$file.'"></script>';
				}
			}
		?>
    </body>
</html>