$.fn.dropdown = function (dropdownHeading = null, dropdownBlock = null){
    var dropdown = $(this);

    if(dropdownHeading == null) {
        dropdownHeading = dropdown.find("> div:nth-child(1)");
    }
    else {
        dropdownHeading = dropdown.find(dropdownHeading);
    }
    if(dropdownBlock == null) {
        dropdownBlock = dropdown.find("> div:nth-child(2)");
    }
    else {
        dropdownBlock = dropdown.find(dropdownBlock);
    }

    dropdownHeading.on('click', function(){
        dropdownBlock.toggle();
        $(this).toggleClass('active');
    });
    $(document).mouseup(function (e){
        if (!dropdown.is(e.target) && dropdown.has(e.target).length === 0) {
            dropdownBlock.hide();
            dropdownHeading.removeClass('active');
        }
    });
}
$.fn.accordion = function(autoClose, hasWrapBlocks = false){
    var accordion = $(this);
    if(hasWrapBlocks == true){
        var accordionHeadings = accordion.find("> div > div:nth-of-type(1)");
    }
    else {
        var accordionHeadings = accordion.find("> div").filter(':even');
    }

    if(autoClose) {
        accordionHeadings.removeClass('active');
        accordionHeadings.next().css('display', 'none');
    }
    else {
        accordionHeadings.addClass('active');
        accordionHeadings.next().css('display', 'block');
    }

    accordionHeadings.each(function(){
        $(this).on('click', function(){
            if(autoClose) {
                accordionHeadings.not(this).removeClass('active');
                accordionHeadings.not(this).next().slideUp();
            }
            $(this).toggleClass('active');
            $(this).next().slideToggle();
        });
    })
}
$.fn.tabs = function(tab = 1) {
    var tabs = $(this);
    var tabsBtns = tabs.find('.tabs-btn');
    var tabsBlocks = tabs.find('.tabs-block');

    $(tabsBtns).removeClass('active');
    $(tabsBlocks).hide();

    $(tabsBtns[tab - 1]).addClass('active');
    $(tabsBlocks[tab - 1]).show();

    $(tabsBtns).each(function (index) {
        $(this).on('click', function(){
            $(this).addClass('active');
            $(this).siblings().removeClass('active');
            $(tabsBlocks[index]).show();
            $(tabsBlocks[index]).siblings().hide();
        })
    });
}
$.fn.modal = function(modal) {
    var modalShow = $(this);
    $(modalShow).on('click', function(e){
        $(modal).fadeIn();
        $(modal).css('display', 'flex');
        // $('body').css('overflow', 'hidden');
    })

    $(document).mouseup(function (e){
        if ($(modal).is(e.target)) {
            $(modal).fadeOut();
            // $('body').css('overflow', 'auto')
        }
        //костыль на datepicker
        // if (!$(modal).is(e.target) && $(modal).has(e.target).length === 0 && !$('.ui-datepicker a').is(e.target) && !$('.ui-datepicker span').is(e.target) && !$('.ui-datepicker div').is(e.target) && !$('.ui-datepicker th').is(e.target) && !$('.ui-datepicker').is(e.target)) {
        //     $(modal).fadeOut();
        // }
    });
}
$.fn.checkAllInputs = function() {
    var checkAllInputs = $(this);
    var checkBtn = checkAllInputs.find('.js-btn-autocheck');
    var inputs = checkAllInputs.find('.js-input-autocheck');
    var labels = checkAllInputs.find('.js-label-autocheck');

    $(checkBtn).on('click', function(e) {
        if (!$('button').is(e.target) && !$('a').is(e.target)) {
            $(inputs).prop('checked',true);
            if ((checkBtn).hasClass('active')) {
                $(inputs).prop('checked',false);
                $(checkBtn).removeClass('active');
            } else {
                $(checkBtn).addClass('active');
            }
        }
    });

    $('input[type="reset"]').on('click', function() {
        $(inputs).prop('checked',false);
        $(checkBtn).removeClass('active');
    });
    
    $(labels).each(function (index) {
        $(this).on('click', function(e) {
            if (!$('button').is(e.target) && !$('a').is(e.target)) {
                var counter = inputs.length;
                $(inputs).each(function() {
                    if($(this).prop('checked')==false) {
                        counter--;
                    }
                });
                if (counter == (inputs.length)) {
                    $(checkBtn).removeClass('active');
                }
                if($(inputs[index]).prop('checked')==false && counter == (inputs.length - 1)) {
                    $(checkBtn).addClass('active');
                }
            }
        });
    });
}
function datepickerGetDate( element ) {
    var date;
    try {
        date = $.datepicker.parseDate( dateFormat, element.value );
    } catch( error ) {
        date = null;
    }
    return date;
}

var dateFormat = "dd.mm.yy";

var datepickerSettings = {
    closeText: "Закрыть",
    prevText: "&#x3C;Пред",
    nextText: "След&#x3E;",
    currentText: "Сегодня",
    monthNames: [ "Январь","Февраль","Март","Апрель","Май","Июнь",
    "Июль","Август","Сентябрь","Октябрь","Ноябрь","Декабрь" ],
    monthNamesShort: [ "Янв","Фев","Мар","Апр","Май","Июн",
    "Июл","Авг","Сен","Окт","Ноя","Дек" ],
    dayNames: [ "воскресенье","понедельник","вторник","среда","четверг","пятница","суббота" ],
    dayNamesShort: [ "вск","пнд","втр","срд","чтв","птн","сбт" ],
    dayNamesMin: [ "Вс","Пн","Вт","Ср","Чт","Пт","Сб" ],
    weekHeader: "Нед",
    dateFormat: dateFormat,
    firstDay: 1,
    showOtherMonths: true,
    selectOtherMonths: true,
    isRTL: false,
}

$(document).ready(function() {
    $('.btn-modal-close').on('click', function () {
        $(this).parents(".modal").fadeOut();
        // $('body').css('overflow', 'auto');
    });


    // HEADER
    $('.header-menu').dropdown();
    $('.header-search').dropdown();
    $('.header-cart').dropdown();
    

    // $('ИНПУТ КАЛЕНДАРЯ').datePicker(datepickerSettings);
});