$(document).ready(function() {
    $('.button-change').modal('.modal-edit-personal-data');
    $('.button-password').modal('.modal-addition-protection');
    $('.change-email-address').modal('.modal-change-email');
    $('.button-phone').modal('.modal-change-phone');
    $('.button-change-password').modal('.modal-change-password');
    $('.button-parametrs').modal('.modal-my-shape-options');
    $('.button-recipient').modal('.modal-recipient');
    $('.button-yandex-map').modal('.modal-yandex-map');
});