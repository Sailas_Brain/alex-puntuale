$(document).ready(function() {
    $('.page-my-appeals .my-appeals').tabs(1);
    $('.page-my-appeals .btn-create-appeal').modal('.modal-create-appeal');
    $('.page-discount .btn-show-modal-calculate-delivery').modal('.modal-calculate-delivery');
    $('.page-my-appeals .my-appeals-list').checkAllInputs();
    $('.page-my-appeals .appeals-archive-list').checkAllInputs();
});