$(document).ready(function() {
    $('.page-my-orders .my-orders').tabs(3);
    $('.page-my-orders .my-orders-list').accordion(true);
    $('.page-my-orders .deliveries-list').accordion(true);
    $('.page-my-orders .btn-cancel-order-request').modal('.modal-cancel-order-request');
    $('.page-my-orders #btn-show-modal-rate-delivery-service').modal('.modal-rate-delivery-service');
    for (let i = 0; i < $('.page-my-orders .deliveries-list .js-wrapper').length; i++) {
        $('.page-my-orders .js-wrapper-' + i).checkAllInputs();
    }
});