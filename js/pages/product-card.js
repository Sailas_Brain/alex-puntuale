$(document).ready(function() {
    $('.card-item-slider__preview-images').slick({
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        arrows: true,
        dots: false,
        vertical: true,
        verticalSwiping: true,
    });
    $('.card-item-slider__preview-images img').on("click", function(){
        var productCardItemPreview = $(this).parents('.card-item-slider');
        $('.card-item-slider__preview-images img').removeClass('active');
        $(this).addClass('active');
        $(productCardItemPreview).find('.card-item-slider__big-preview img').attr('src', $(this).attr("src"));
    });
    $(document).find('.page-product-card .item-color-input').on('click', function(){
        $('.card-item-info__itemcode').text($(this).attr('data-code'));
        $('.card-item-info__itemcolor').text($(this).attr('data-color'));
    })
    $('.page-product-card .card-item-info__itemcode').text($('.page-product-card .item-color-input').attr('data-code'));
    $('.page-product-card .card-item-info__itemcolor').text($('.page-product-card .item-color-input').attr('data-color'));
    $('.page-product-card .card-communication').tabs(1);
    $('.page-product-card .btn-show-modal-inaccurate-description').modal('.modal-inaccurate-description');
    $('.page-product-card .btn-show-modal-found-cheaper').modal('.modal-found-cheaper');
    $('.page-product-card .btn-show-modal-find-your-size').modal('.modal-find-your-size');

    $('.card-products-slider__block').slick({
        infinite: true,
        slidesToShow: 5,
        slidesToScroll: 1,
        arrows: true,
        dots: false,
        prevArrow: '<button type="button" class="btn products-slider__btn products-slider__btn-prev"></button',
        nextArrow: '<button type="button" class="btn products-slider__btn products-slider__btn-next"></button',
    });
});