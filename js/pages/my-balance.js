$(document).ready(function() {
    $('.button-detail').modal('.modal-gift-item-details');
    $('.button-certificate').modal('.modal-your-certificate');

    var datepickerTo = $('#datepicker-to').datepicker(datepickerSettings).on( "change", function() {
        datepickerFrom.datepicker( "option", "maxDate", datepickerGetDate( this ) );
    });
    var datepickerFrom = $('#datepicker-from').datepicker(datepickerSettings).on( "change", function() {
        datepickerTo.datepicker( "option", "minDate", datepickerGetDate( this ) );
    });
    

    $('.my-balance__operations-history').dropdown();
});