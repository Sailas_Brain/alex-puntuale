$(document).ready(function() {
    $('.page-discount .btn-show-modal-calculate-discount').modal('.modal-calculate-discount');
    $('.page-discount .btn-show-modal-calculate-buyout').modal('.modal-calculate-buyout');
    $('.page-discount .btn-show-modal-calculate-delivery').modal('.modal-calculate-delivery');
    $('.modal-calculate-buyout .info-block .dropdown-container').dropdown('.btn-show-calculator', '.dropdown-block');
    $('.page-discount .accordion').accordion(true);
});