$(document).ready(function() {
    $('.catalog-filter').accordion(false, true);

    $(".products-list-item__btn-quick-view, .products-list-item__btn-add-to-favourites, .products-list-item__btn-add-to-cart, .products-list-item__rating").on("click", function(e) {
        e.preventDefault();
    });
    $('.products-list-item__preview-images img').on("mouseenter", function(){
        var catalogProductsItem = $(this).parents('.products-list-item');
        
        $('.products-list-item__preview-images img').removeClass('active');
        $(this).addClass('active');
        $(catalogProductsItem).find('.products-list-item__image').attr('src', $(this).attr("src"));
    });

    $('.catalog-products-slider__block').slick({
        infinite: false,
        slidesToShow: 5,
        slidesToScroll: 2,
        arrows: true,
        dots: false,
        prevArrow: '<button type="button" class="btn products-slider__btn products-slider__btn-prev"></button',
        nextArrow: '<button type="button" class="btn products-slider__btn products-slider__btn-next"></button',
    });




    var catalogFilterPriceSlider = $('.catalog-filter__price-slider').slider({
        range: true,
        min: 0,
        max: 2500,
        values: [ 0, 2500 ],
        slide: function( event, ui ) {
            $('.catalog-filter__price-val-min').val(ui.values[0]);
            $('.catalog-filter__price-val-max').val(ui.values[1]);
        }
    });
    $('.catalog-filter__price-val').on('change', function() {
        var catalogFilterPriceValues = Array();
        $('.catalog-filter__price-val').each(function(){
            catalogFilterPriceValues.push($(this).val());
        })
        catalogFilterPriceSlider.slider('option', 'values', catalogFilterPriceValues);
        delete catalogFilterPriceValues;
    });

    var catalogFilterHeightSlider = $('.catalog-filter__height-slider').slider({
        range: true,
        min: 0,
        max: 200,
        values: [ 0, 200 ],
        slide: function( event, ui ) {
            $('.catalog-filter__height-val-min').val(ui.values[0]);
            $('.catalog-filter__height-val-max').val(ui.values[1]);
        }
    });
    $('.catalog-filter__height-val').on('change', function() {
        var catalogFilterHeightValues = Array();
        $('.catalog-filter__height-val').each(function(){
            catalogFilterHeightValues.push($(this).val());
        })
        catalogFilterHeightSlider.slider('option', 'values', catalogFilterHeightValues);
        delete catalogFilterHeightValues;
    });


    $('.catalog-products-sort').dropdown('.catalog-products-sort__heading span', '.catalog-products-sort__block');
});