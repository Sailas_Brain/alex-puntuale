<a href="#" class="products-list-item">
    <div class="products-list-item__hover">
        <div class="products-list-item__preview-images">
            <img src="/images/dynamic/product-image-1.jpg" alt="">
            <img src="/images/dynamic/product-image-2.jpg" alt="">
            <img src="/images/dynamic/product-image-3.jpg" alt="">
        </div>
        <div class="products-list-item__sizes">
            <span>Размеры в наличии</span>
            <span>37, 38, 39, 40, 41, 42, 43, 44, 45</span>
        </div>
    </div>
    
    <div class="products-list-item__inner">
        <img class="products-list-item__image" src="/images/dynamic/product-image-1.jpg" alt="">
        <div class="products-list-item__info">
            <div href="" class="products-list-item__name">Рубашка женская</div>
            
            <div class="products-list-item__price"><span>4200 руб.</span><span>4200 руб.</span></div>

            <div class="products-list-item__rating">
                <div class="products-list-item__rating-stars"></div>
                <span class="products-list-item__rating-score">(5)</span>
            </div>

            <div class="products-list-item__price-promo">4 006 руб. <span>по промокоду</span></div>

            <button class="btn products-list-item__btn-add-to-cart"></button>
            <button class="btn products-list-item__btn-add-to-favourites"><i class="far fa-heart"></i></button>

            <span class="products-list-item__mark products-list-item__mark-hit">ХИТ</span>
            <span class="products-list-item__mark products-list-item__mark-sale">SALE</span>

            <button class="btn products-list-item__btn-quick-view">Быстрый просмотр</button>
            <span class="products-list-item__discount">-50%</span>
        </div>
    </div>
</a>