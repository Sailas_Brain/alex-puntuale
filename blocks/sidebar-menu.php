<div class="sidebar-menu">
    <ul class="sidebar-menu__list">
        <li>
            <a href="">Товары</span></a>
            <ul>
                <li><a href="/?p=cart">Моя корзина</a></li>
                <li><a href="/?p=postponed-items">Отложенные товары</a></li>
                <li><a href="/?p=waiting-list">Лист ожидания</a></li>
            </ul>
        </li>
        <li>
            <a href="">Работа с заказом</span></a>
            <ul>
                <li><a href="/?p=my-orders">Мои заказы</a></li>
                <li><a href="/?p=my-orders">Мои доставки</a></li>
                <li><a href="">Мой баланс</a></li>
                <li><a href="">Личные предложения</a></li>
                <li><a href="/?p=my-wardrobe">Гардероб/возвраты</a></li>
                <li><a href="/?p=my-wardrobe">Оформление возврата</a></li>
                <li><a href="">Отслеживание по треку</a></li>
            </ul>
        </li>
        <li>
            <a href="/?p=my-account">Профиль</span></a>
            <ul>
                <li><a href="">Мои данные</a></li>
                <li><a href="/?p=discount">Моя скидка <span>5%</span></a></li>
                <li><a href="">Мои рассылки</a></li>
                <li><a href="">Новости</a></li>
            </ul>
        </li>
        <li>
            <a href="">Обратная связь</span></a>
            <ul>
                <li><a href="/?p=my-appeals">Мои обращения</a></li>
                <li><a href="">Качество обслуживания</a></li>
                <li><a href="">Вопросы к товару</a></li>
                <li><a href="">Проверка товара</a></li>
            </ul>
        </li>
        <li>
            <a href="">Социальные</span></a>
            <ul>
                <li><a href="">Мои отзывы</a></li>
                <li><a href="">Выйти</a></li>
            </ul>
        </li>
    </ul>
</div>