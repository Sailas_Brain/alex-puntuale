<div class="footer">
    <div class="footer-line">
        <div class="footer__inner">
            <div class="col-1">
                <div class="footer-logo">
                    <img src="/images/footer/logo-big.svg" alt="">
                </div>
                <div class="footer-contacts">
                    <span>Москва</span>
                    <span>проспект Чекистов, 650</span>
                    <a href="#">puntuale@mail.ru</a>
                    <a href="#">8 800 000 00 00 </a>
                </div>
                <div class="footer-copyright">
                    <span>© 2018 Puntuale - Все права защищены.</span>
                </div>
            </div>
            <div class="col-2">
                <div class="footer-menu">
                    <span class="footer-menu__heading">Сервис и поддержка</span>
                    <ul class="footer-menu__list">
                        <li><a href="">Как сделать заказ</a></li>
                        <li><a href="">Возврат товара</a></li>
                        <li><a href="">Доставка</a></li>
                        <li><a href="">Способы оплаты</a></li>
                        <li><a href="">Возврат денежных средств</a></li>
                        <li><a href="">Вопросы и ответы</a></li>
                        <li><a href="">Публичная оферта</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-3">
                <div class="footer-menu">
                    <span class="footer-menu__heading">О компании</span>
                    <ul class="footer-menu__list">
                        <li><a href="">О нас</a></li>
                        <li><a href="">Контакты</a></li>
                        <li><a href="">Сертификаты</a></li>
                        <li><a href="">Преимущества</a></li>
                        <li><a href="">Наши скидки</a></li>
                        <li class="active"><a href=""><img src="/images/static/icon-gift-card.png" alt="">Подарочные карты</a></li>
                        <li><a href="">Фабрика</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-4">
                <div class="footer-subscribe">
                    <div class="footer-subscribe__heading">Подписка</div>
                    <div class="footer-subscribe__text">Подпишитесь, чтобы быть в курсе новостей и скидок</div>
                    <form action="" class="footer-subscribe__form">
                        <input type="email" required placeholder="Ваш e-mail">
                        <button><i class="fas fa-angle-right"></i></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-line">
        <div class="footer__inner">
            <ul class="footer-socials">
                <li><a href=""><i class="fab fa-viber"></i></a></li>
                <li><a href=""><i class="fab fa-whatsapp"></i></a></li>
                <li><a href=""><i class="fab fa-facebook-f"></i></a></li>
                <li><a href=""><i class="fab fa-vk"></i></a></li>
                <li><a href=""><i class="fab fa-telegram-plane"></i></a></li>
                <li><a href=""><i class="fab fa-twitter"></i></a></li>
                <li><a href=""><i class="fab fa-youtube"></i></a></li>
                <li><a href=""><i class="fab fa-instagram"></i></a></li>
            </ul>

            <div class="footer-links">
                <a href="" class="footer-call">
                    <span>Звонок с сайта</span>
                    <i class="fas fa-phone"></i>
                </a>
                <a href="" class="footer-pickup">
                    <span>Бесплатная доставка</span>
                    <i class="fas fa-box"></i>
                </a>
                <a href="" class="footer-faq">
                    <span>Пункты самовывоза</span>
                    <i class="fas fa-map-marker-alt"></i>
                </a>
                <a href="" class="footer-faq">
                    <span>Частые вопросы</span>
                    <i class="fas fa-question"></i>
                </a>
            </div>
        </div>
    </div>
</div>