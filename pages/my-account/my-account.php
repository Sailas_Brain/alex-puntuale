<div class="page page-my-account">
    <div class="page__inner page-my-account__inner">
        <div class="col-1">
            <?include('blocks/sidebar-menu.php');?>
        </div>
        <div class="col-2">
            <h1>Личный кабинет</h1>
            <div class="my-account-top">
                <div class="my-account-top__col-1">
                    <div class="my-cart-card">
                        <b>Моя корзина</b>
                        <b>2 товара</b>
                        <b>4 014 руб.</b>
                        <a href="">подробнее</a>
                    </div>
                    <div class="my-deliveries-card">
                        <b>Мои доставки</b>
                        <b>Ближайшая: не ожидается</b>
                        <a href="">подробнее</a>
                    </div>
                </div>
                <div class="my-account-top__col-2">
                    <div class="my-balance-card">
                        <div class="my-balance-card__header row">
                            <b>Мой баланс</b>
                            <a class="default-link" href="">Подробнее</a>
                        </div>
                        <div class="my-balance-card__available-cash">
                            <b>Доступный остаток</b>
                            <b>0 руб.</b>
                        </div>
                        <div class="my-balance-card__balance">
                            <div class="row">
                                <b>Баланс</b>
                                <a class="default-link" href="">Пополнить счёт</a>
                            </div>
                            <b class="value">0 руб.</b>
                        </div>
                        <div class="my-balance-card__frozen-balance">
                            <div class="row">
                                <b>Заблокировано</b>
                                <a class="default-link" href="">Детализация</a>
                            </div>
                            <b class="value">0 руб.</b>
                        </div>
                        <div class="my-balance-card__action-installments">
                            <div class="row">
                                <b>Акция «Рассрочка»</b>
                                <a class="default-link" href="">Подробнее</a>
                            </div>
                            <a class="default-link" href="">Заполнить анкету</a>
                        </div>
                        <a class="my-balance-card__operation-history row" href="/?p=my-balance">
                            <b>История операций</b>
                            <i></i>
                        </a>
                        <a class="my-balance-card__gift-certificate-activation row" href="/?p=my-balance">
                            <b>Активация подарочного сертификата</b>
                            <i></i>
                        </a>
                    </div>
                </div>
                <div class="my-account-top__col-3">
                    <div class="my-orders-card">
                        <b>Мои заказы</b>
                        <a class="default-link" href="">Подробнее</a>
                    </div>
                    <div class="waiting-list-card">
                        <b>Лист ожидания</b>
                        <b>2 товара</b>
                        <b>Доступно к заказу: 0</b>
                        <a class="default-link" href="">Подробнее</a>
                    </div>
                    <div class="pending-items-card">
                        <b>Отложенные товары</b>
                        <b>3 товара</b>
                        <a class="default-link" href="">Подробнее</a>
                    </div>
                </div>
            </div>
            <div class="my-account-mid">
                <div class="my-account-mid__col-1">
                    <div class="info">
                        <div class="info__my-discount">
                            <b class="info__discount-title">Моя скидка</b>
                            <b class="info__discount-percentage">0<span class="info__superscript">%</span></b>
                        </div>
                        <div class="info__right-col">
                            <p>Процент выкупа: <span>22,25%</span></p>
                            <p>Сумма выкупа: <span>29 553 руб.</span></p>
                            <p><b class="item-amount">Кол-во товаров:</b><b class="description-popup">Кол-во товаров, которое можно оформить на доставку без предварительной оплаты</b><span> до 30 шт.</span></p>
                            <a href="">Подробнее</a>
                        </div>
                    </div>
                </div>
                <div class="my-account-mid__col-2">
                    <div class="personal-info-card">
                        <div class="personal-info-card__picture-column">
                            <img src="" alt="">
                            <form>
                                <input id="edit-profile-picture" type="file">
                                <label class="btn btn-edit-profile-picture" for="edit-profile-picture"></label>
                            </form>
                        </div>
                        <div class="personal-info-card__info-column">
                            <b class="user-name">Щербина Кристина</b>
                            <p>E-mail:<b>kristina89@bk.ru</b></p>
                            <p>Дата рождения:<b>23.05.1989</b></p>
                            <p>Телефон:<b>+7 (965) 242-29-99</b></p>
                            <a class="default-link" href="">Посмотреть все даннные</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="my-account-bottom">
                <div class="my-account-bottom__col-1">
                    <div class="bonuses-card">
                        <h2>Бонусы</h2>
                        <p>Бонусами можно оплатить 100% стоимости заказа. Начисление бонусов происходит через 1 час после покупки товара. Размер бонуса виден в Корзине и в карточке товара. Детализация начисленных бонусов доступна в разделе <a href="">«<span>Мой баланс</span>»</a>.</p> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>