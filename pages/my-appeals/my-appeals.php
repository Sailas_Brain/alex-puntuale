<div class="page page-my-appeals">
    <div class="page__inner page-my-appeals__inner">
        <div class="col-1">
            <?include('blocks/sidebar-menu.php');?>
        </div>
        <div class="col-2">
            <h1>Мои обращения</h1>
            <div class="my-appeals tabs">
                <div class="my-appeals__header-list">
                    <div class="tabs-btn active"><span>Мои обращения</span></div>
                    <div class="tabs-btn"><span>Архив обращений</span></div>
                    <div class="search"><label><input type="text" placeholder="Найти"><i class="fas fa-search"></i></label></div>
                </div>
                <div class="my-appeals__tabs-container"> <!-- maybe this container isn't needed -->
                    <div class="my-appeals-tab tabs-block">
                        <form action="" name="my-appeals-form">
                            <div class="my-appeals-tab__top-menu">
                                <button>Переместить в архив</button>
                                <button class="btn btn-create-appeal" type="button">Создать обращение</button>
                                <i class="fas fa-question-circle"></i><a href="">Ответы на часто задаваемые вопросы</a>
                            </div>
                            <div class="my-appeals-tab__top-nav">
                                <p>Заказов <span>3</span></p>
                                <ul class="catalog-products-pagination">
                                    <li><a href=""><i class="fas fa-angle-left"></i></a></li>
                                    <li class="catalog-products-pagination__item"><a href="">1</a></li>
                                    <li class="active"><a href="">2</a></li>
                                    <li><a href="">3</a></li>
                                    <li><a href=""><i class="fas fa-angle-right"></i></a></li>
                                </ul>
                            </div>
                            <div class="my-appeals-list">
                                <ul class="my-appeals-list__list-heading">
                                    <li class="select-all-inputs js-btn-autocheck"><i class="indicator"></i>Выделить все №</li>
                                    <li>Раздел</li>
                                    <li>Доп. параметры</li>
                                    <li>Дата</li>
                                </ul>
                                <ul class="my-appeals-list__appeal-list">
                                    <? for ($i=0; $i < 4; $i++) { ?>
                                    <li>
                                        <input class="js-input-autocheck" type="checkbox" name="move-to-archive" value="24977875" id="appeal-id-<?echo($i)?>">
                                        <label class="js-label-autocheck" for="appeal-id-<?echo($i)?>">
                                            <div class="column"><b>24977875</b></div>
                                            <div class="column"><b>Сложность в оформлении заказа</b></div>
                                            <div class="column"><b>Промокод: 65675DF4</b></div>
                                            <div class="column"><b>21.11.2017</b></div>
                                        </label>
                                    </li>
                                    <? } ?>
                                </ul>
                            </div>
                            <div class="my-appeals-tab__bottom-nav">
                                <ul class="catalog-products-pagination">
                                    <li><a href=""><i class="fas fa-angle-left"></i></a></li>
                                    <li class="catalog-products-pagination__item"><a href="">1</a></li>
                                    <li class="active"><a href="">2</a></li>
                                    <li><a href="">3</a></li>
                                    <li><a href=""><i class="fas fa-angle-right"></i></a></li>
                                </ul>
                            </div>
                        </form>
                    </div>
                    <div class="appeals-archive-tab tabs-block">
                        <form action="" name="appeals-archive-form">
                            <div class="appeals-archive-tab__top-menu">
                                <button class="btn btn-create-appeal" type="button">Создать обращение</button>
                                <i class="fas fa-question-circle"></i><a href="">Ответы на часто задаваемые вопросы</a>
                            </div>
                            <div class="appeals-archive-tab__top-nav">
                                <p>Заказов <span>3</span></p>
                                <ul class="catalog-products-pagination">
                                    <li><a href=""><i class="fas fa-angle-left"></i></a></li>
                                    <li class="catalog-products-pagination__item"><a href="">1</a></li>
                                    <li class="active"><a href="">2</a></li>
                                    <li><a href="">3</a></li>
                                    <li><a href=""><i class="fas fa-angle-right"></i></a></li>
                                </ul>
                            </div>
                            <div class="appeals-archive-list">
                                <ul class="appeals-archive-list__list-heading">
                                    <li class="select-all-inputs js-btn-autocheck"><i class="indicator"></i>Выделить все №</li>
                                    <li>Раздел</li>
                                    <li>Доп. параметры</li>
                                    <li>Дата</li>
                                </ul>
                                <ul class="appeals-archive-list__appeal-list">
                                    <? for ($i=0; $i < 4; $i++) { ?>
                                    <li>
                                        <input class="js-input-autocheck" type="checkbox" name="archived-appeal" value="24977875" id="archived-appeal-id-<?echo($i)?>">
                                        <label class="js-label-autocheck" for="archived-appeal-id-<?echo($i)?>">
                                            <div class="column"><b>24977875</b></div>
                                            <div class="column"><b>Сложность в оформлении заказа</b></div>
                                            <div class="column"><b>Промокод: 65675DF4</b></div>
                                            <div class="column"><b>21.11.2017</b></div>
                                        </label>
                                    </li>
                                    <? } ?>
                                </ul>
                            </div>
                            <div class="appeals-archive-tab__bottom-nav">
                                <ul class="catalog-products-pagination">
                                    <li><a href=""><i class="fas fa-angle-left"></i></a></li>
                                    <li class="catalog-products-pagination__item"><a href="">1</a></li>
                                    <li class="active"><a href="">2</a></li>
                                    <li><a href="">3</a></li>
                                    <li><a href=""><i class="fas fa-angle-right"></i></a></li>
                                </ul>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>