<div class="page page-auth">
    <div class="page__inner page-auth__inner">
        <div class="col-1">
            <div class="auth">
                <h1>Вход & Регистрация</h1>
                <div class="auth__nav">
                    <a href="">Главная</a>
                    <i class="fas fa-arrow-right"></i>
                    <a href="">Вход & Регистрация</a>
                </div>
                <div class="auth__header-list">
                    <div class="auth-switch tabs-btn active"><span>Войти</span></div>
                    <i></i>
                    <div class="register-switch tabs-btn"><span>Зарегистрироваться</span></div>
                </div>
                <div class="auth__tabs-container">
                    <div class="auth-tab tabs-block">
                        <form action="" name="auth-form">
                            <input class="default-input" type="text" placeholder="E-mail или логин или телефон">
                            <input class="default-input" type="password" placeholder="Пароль">
                            <div class="row">
                                <div>
                                    <input type="checkbox" id="remember-me">
                                    <label for="remember-me">Запомнить меня</label>
                                </div>
                                <a href="">Забыли пароль?</a>
                            </div>
                            <b class="additional-security">Дополнительная защита входа <b class="additional-security__popup">Обеспечивает надежный вход в Личный кабинет. Для входа на страницу необходимо ввести одноразовый код, полученный по СМС.</b></b>
                            <input type="submit" name="login" value="Войти">
                            <div class="auth-with-social">
                                <b>Войти с помощью:</b>
                                <a href=""><i class="fab fa-facebook-f"></i></a>
                                <a href=""><i class="fab fa-twitter"></i></a>
                                <a href=""><i class="fab fa-google-plus-g"></i></a>
                                <a href=""><i class="fab fa-instagram"></i></a>
                                <a href=""><i class="fab fa-vk"></i></a>
                                <a href=""><i class="fas fa-at"></i></a>
                                <a href=""><i class="fab fa-odnoklassniki"></i></a>
                            </div>
                            <b class="how-it-works">Как это работает?<b class="how-it-works__popup">Вы можете связать свой аккаунт в соц.сетях с личным кабинетом Puntuale. Это удобно и абсолютно безопасно.</b></b>
                        </form>
                    </div>
                    <div class="register-tab tabs-block">
                        <form action="" name="register-form">
                            <div class="register-tab__col-1">
                                <input class="default-input" type="text" name="username" placeholder="Имя*">
                                <input class="default-input" type="passowrd" name="password" placeholder="Пароль*">
                                <input class="default-input" type="passowrd" name="password-confirm" placeholder="Подтвердите пароль*">
                                <input type="checkbox" id="subscribe-email" name="subscribe-email" checked>
                                <label for="subscribe-email">Я хочу получать эксклюзивные скидки и подарки</label>
                            </div>
                            <div class="register-tab__col-2">
                                <input class="default-input" type="email" name="email" placeholder="E-mail*">
                                <div class="row tel-container">
                                    <select name="" id="">
                                        <option value="7">+7</option>
                                    </select>
                                    <!-- <input type="number" name="tel-first" placeholder="Тел +7*"> -->
                                    <input type="tel" name="tel-other">
                                </div>
                                <div class="row">
                                    <input class="short-input" type="text" name="sms-code" placeholder="Код из СМС*">
                                    <button class="submit-sms-code" type="button">Отправить код</button>
                                </div>
                                <div class="row">
                                    <label class="datepicker__label">
                                        <input class="datepicker" type="text" id="datepicker" name="birthday" placeholder="Дата рождения">
										<i class="datepicker__icon"></i>
									</label>
                                    <b>Мы побалуем вас подарком</b>
                                </div>
                            </div>
                            <div class="register-tab__col-3">
                                <input type="submit" value="Зарегистрироваться">
                                <p>Нажимая кнопку «Зарегистрироваться», я соглашаюсь с условиями <a href="">Публичной оферты</a> <br>*поля, обязательные для заполнения</p>
                                <div class="auth-with-social">
                                    <b>Зарегистрироваться через:</b>
                                    <a href=""><i class="fab fa-facebook-f"></i></a>
                                    <a href=""><i class="fab fa-twitter"></i></a>
                                    <a href=""><i class="fab fa-google-plus-g"></i></a>
                                    <a href=""><i class="fab fa-instagram"></i></a>
                                    <a href=""><i class="fab fa-vk"></i></a>
                                    <a href=""><i class="fas fa-at"></i></a>
                                    <a href=""><i class="fab fa-odnoklassniki"></i></a>
                                </div>
                                <b class="how-it-works">Как это работает?<b class="how-it-works__popup">Вы можете связать свой аккаунт в соц.сетях с личным кабинетом Puntuale. Это удобно и абсолютно безопасно.</b></b>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>