<div class="header">
    <div class="header__inner header__inner_home">
        <div class="header__line">
            <div class="col-1">
                <a class="header-logo" href="/">
                    <img src="images/header/logo-small.svg" alt="">
                </a>
                <div class="header-menu">
                    <div class="header-menu__toggle">
                        <i class="header-menu__icon"></i>
                        <span>Меню</span>
                    </div>
                    <div class="header-menu__block">
                        <ul>
                            <li class="active"><a href="/">Главная</a></li>
                            <li><a href="/">О нас</a></li>
                            <li><a href="/">Информация</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-2">
                <div class="header-cart">
                    <div class="header-cart__heading">
                        <i></i>
                        <div>Корзина - <span class="header-cart-qty">01</span></div>
                    </div>
                    <div class="header-cart__block">
                        <a href="/" class="header-cart-item">
                            <div class="header-cart-item__left">
                                <div class="header-cart-item__img">
                                    <img src="/images/dynamic/product-image-1.jpg" alt="">
                                    <div class="header-cart-item__label">ХИТ</div>
                                </div>
                            </div>
                            <div class="header-cart-item__right">
                                <span class="header-cart-item__title">Рубашка мужская</span>
                                <div class="header-cart-item__prop">
                                    <span class="header-cart-item__prop-name">Артикул:</span>
                                    <span class="header-cart-item__prop-value">3114961</span>
                                </div>
                                <div class="header-cart-item__prop">
                                    <span class="header-cart-item__prop-name">Цвет:</span>
                                    <span class="header-cart-item__prop-value">Черный</span>
                                </div>
                                <div class="header-cart-item__prop">
                                    <span class="header-cart-item__prop-name">Размер:</span>
                                    <span class="header-cart-item__prop-value">5</span>
                                </div>
                                <div class="header-cart-item__prop">
                                    <span class="header-cart-item__prop-name">Кол-во:</span>
                                    <span class="header-cart-item__prop-value">1</span>
                                </div>
                                <div class="header-cart-item__price"><span>2.400</span> руб.</div>
                            </div>
                        </a>
                        <div class="header-cart-summary">
                            <div class="header-cart-summary__left">
                                <span>Итого</span>
                            </div>
                            <div class="header-cart-summary__right">
                                <span>3900</span> руб
                            </div>
                        </div>
                        <div class="btn-group header-cart-btn-group">
                            <button class="btn header-cart__btn btn-go-to-cart">В корзину</button>
                            <button class="btn header-cart__btn btn-go-to-order">Оформить</button>
                        </div>
                    </div>
                </div>

                <div class="header-search">
                    <div class="header-search__heading">
                        <i></i>
                    </div>
                    <div class="header-search__block">
                        <form action="">
                            <input type="text" placeholder="Найти">
                            <button><i></i></button>
                        </form>
                    </div>
                </div>

                <div class="header-auth-control">
                    <div class="header-auth-control__heading">
                        <a href="/?p=auth">Регистрация / Войти</a>
                        <i></i>
                    </div>
                    <div class="header-auth-control__block">
                        <span>user</span>
                    </div>
                </div>

                <a href="tel:1" class="header-tel">
                    <span>8 800 800 00 00</span>
                    <i></i>
                </a>
            </div>
        </div>
        <div class="header__line header__line_home">
            <div class="col-3 col-3_home">
                <div class="home-header-service-buttons">
                    <div class="home-header-service-buttons__heading">
                        <span>Добро пожаловать в пунтуале</span>
                        <div>
                            <span>Итальянские сорочки</span>
                            <span>От портного</span>
                        </div>
                        <span>Создай свой уникальный дизайн сорочки</span>
                    </div>
                    <div class="home-header-service-buttons__group">
                        <button class="btn btn-call-tailor">Вызвать портного</button>
                        <button class="btn btn-create-shirt">Создать рубашку</button>
                    </div>
                </div>
            </div>
            <div class="col-4 col-4_home">
                <div class="home-header-catalog">
                    <div class="home-header-catalog__heading">
                        <span>Каталог</span>
                        <i></i>
                    </div>
                    <ul class="home-header-catalog__list">
                        <li>
                            <a href="">Рубашка</a>
                            <ul>
                                <li><a href="">Classic</a></li>
                                <li><a href="">Casual</a></li>
                            </ul>
                        </li>
                        <li><a href="">Бабочки</a></li>
                        <li><a href="">Галстуки</a></li>
                        <li><a href="">Подтяжки</a></li>
                        <li><a href="">Носки</a></li>      
                        <li><a href="">Запонки</a></li>       
                        <li><a href="">Сумки</a></li>       
                        <li><a href="">Портмоне</a></li>
                        <li><a href="">Шейные платки </a></li>
                        <li><a href="">Платки паше</a></li>
                        <li><a href="">Зажимы</a></li>
                        <li><a href="">Булавки</a></li>
                        <li><a href="">Броши</a></li>      
                        <li><a href="">Шарфы</a></li>
                    </ul>
                </div>

                <div class="home-header-communication">
                    <button class="btn btn-location">
                        <div class="popup">Адрес</div>
                    </button>
                    <button class="btn btn-mail">
                        <div class="popup">Email</div>
                    </button>
                    <button class="btn btn-tel">
                        <div class="popup">Телефон</div>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>