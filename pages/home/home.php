<div class="page page-home">
    <div class="page-line banner">
        <div class="page-line__inner banner__inner">
            <a href="">В каталог</a>
        </div>
    </div>
    <div class="page-line featured">
        <div class="page-line__inner featured__inner">
            <div class="featured-item featured-item-1">
                <img src="/images/home/featured-1.png" alt="">
                <div class="featured-item-1__text">
                    <span>Новая коллекция</span>
                    <span>Новые Аксессуары</span>
                    <span>Nam ac elit a ante commodo</span>
                    <a href="">В каталог</a>
                </div>
            </div>
            <div class="featured-item featured-item-2">
                <img src="/images/home/featured-2.png" alt="">
                <div class="featured-item-2__text">
                    <span>Брючный сезон</span>
                    <span>Новая коллекция</span>
                    <a href="">В каталог</a>
                </div>
            </div>
            <div class="featured-item featured-item-3">
                <img src="/images/home/featured-3.png" alt="">
                <div class="featured-item-3__text">
                    <span>Натуральные ткани</span>
                    <br>
                    <span>Новая коллекция</span>
                    <br>
                    <a href="">В каталог</a>
                </div>
            </div>
            <a href="" class="featured-show-all">
                <span>Смотреть все товары</span>
                <i class="fas fa-angle-right"></i>
            </a>
            <div class="featured-item featured-item-4">
                <img src="/images/home/featured-4.png" alt="">
                <div class="featured-item-4__text">
                    <span>Новая коллекция</span>
                    <span>Модные рубашки</span>
                    <span>Nam ac elit a ante commodo</span>
                    <a href="">В каталог</a>
                </div>
            </div>
        </div>
    </div>
    <div class="page-line services">
        <div class="page-line__inner services__inner">
            <div class="services-item">
                <div class="services-item__image">
                    <img src="/images/home/service-1.png" alt="">
                </div>
                <div class="services-item__text">
                    <span>Уникально и на заказ</span>
                    <span>по вашему эскизу</span>
                </div>
            </div>
            <div class="services-item">
                <div class="services-item__image">
                    <img src="/images/home/service-2.png" alt="">
                </div>
                <div class="services-item__text">
                    <span>Бесплатная доставка</span>
                    <span>по всей России</span>
                </div>
            </div>
            <div class="services-item">
                <div class="services-item__image">
                    <img src="/images/home/service-3.png" alt="">
                </div>
                <div class="services-item__text">
                    <span>Вызов портного</span>
                    <span>бесплатно</span>
                </div>
            </div>
            <div class="services-item">
                <div class="services-item__image">
                    <img src="/images/home/service-4.png" alt="">
                </div>
                <div class="services-item__text">
                    <span>Минимальный срок</span>
                    <span>пошива на заказ</span>
                </div>
            </div>
        </div>
    </div>
    <div class="page-line how-it-works">
        <div class="page-line__inner how-it-works__inner">
            <div class="how-it-works__heading">
                <i></i>
                <div>
                    <span>Порядок действий</span>
                    <span>Как это работает?</span>
                </div>
            </div>
            <div class="how-it-works__block">
                <div class="how-it-works-item">
                    <img src="/images/home/how-it-works-1.png" alt="">
                    <span>Создаем дизайн сорочки</span>
                    <a href="">В нашем конструкторе</a>
                </div>
                <div class="how-it-works-item">
                    <img src="/images/home/how-it-works-2.png" alt="">
                    <span>Выбираем ткани</span>
                    <a href="">Как выбрать?</a>
                </div>
                <div class="how-it-works-item">
                    <iframe width="300" height="300" src="https://www.youtube.com/embed/w77zPAtVTuI" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen=""></iframe>
                    <span>Снимаем мерки</span>
                    <span>Инструкция в видео</span>
                </div>
            </div>
        </div>
    </div>
    <div class="page-line instawidget">
        <div class="page-line__inner instawidget__inner">
            <div class="instawidget__heading">
                <span>Наш Instagram</span>
                <span>
                    <i class="fab fa-instagram"></i>
                    <a href="">@puntuale_shop</a>
                </span>
            </div>
            <div class="instawidget__block">
                <!-- InstaWidget -->
                <a href="https://instawidget.net/v/user/puntuale" id="link-1c6155f6036bc35d9539511e197b4e431dbda6264d559a28f219c83a6b939c31">@puntuale</a>
                <script src="https://instawidget.net/js/instawidget.js?u=1c6155f6036bc35d9539511e197b4e431dbda6264d559a28f219c83a6b939c31&width=1200px"></script>
            </div>
        </div>
    </div>
    <div class="page-line page-text">
        <div class="page-line__inner page-text__inner">
            <div class="page-text__section">
                <div class="page-text__heading">
                    <span>Широкий ассортимент и высокое качество</span>
                </div>
                <div class="page-text__block">
                    <p>
                        Интернет магазин одежды Puntuale – это доступные цены, широкий, регулярно обновляемый ассортимент. В онлайн-каталоге Puntuale представлено около 7000 ведущих брендов женской, мужской и детской одежды и обуви, электроники, книжной продукции, ювелирных изделий, игрушек и т.д. Для удобства пользования онлайн-каталог поделен на разделы, все товары можно сортировать по ряду критериев: цена, материал изготовления, сезонность, бренд.
                    </p>
                </div>
            </div>
            <div class="page-text__section">
                <div class="page-text__heading">
                    <span>Выгодный шоппинг</span>
                </div>
                <div class="page-text__block">
                    <p>
                        Интернет магазин Puntuale регулярно проводит масштабные распродажи, в рамках которых купить одежду и обувь становится еще реальнее. Чтобы быть в курсе предстоящих скидок или появления в ассортименте новых моделей одежды от любимых брендов, достаточно подписаться на email-рассылку магазина. Дополнительные выгодные условия действуют для постоянных покупателей Puntuale – персональная скидка, зависящая от процента выкупа вещей. В Puntuale всегда ответственно подходят к выбору поставщиков, со многими производителями мы работаем напрямую, поэтому все категории товаров отличаются высоким качеством, разнообразием моделей и цветов.
                    </p>
                </div>
            </div>
            <div class="page-text__section">
                <div class="page-text__heading">
                    <span>Доставка и оплата без проблем</span>
                </div>
                <div class="page-text__block">
                    <p>
                        Онлайн магазин Puntuale осуществляет бесплатную доставку по всей России с помощью собственной курьерской службы. Также покупатель может забрать заказ из пункта самовывоза или получить по почте. Любую одежду, обувь и другие товары можно примерить перед оплатой заказа курьеру или в пункте самовывоза, оборудованном удобными примерочными.
                    </p>
                    <p>
                        Puntuale предлагает несколько различных вариантов оплаты заказа как при оформлении, так и по факту при получении, - банковскими картой или переводом, наличным расчетом или электронным платежом. Если товар не подошел, его можно вернуть с курьером как до оплаты заказа, так и после по почте или в одном из пунктов самовывоза в течение 21 дня.
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>