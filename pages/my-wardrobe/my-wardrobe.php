<div class="page page-my-wardrobe">
    <div class="page__inner page-my-wardrobe__inner">
        <div class="col-1">
        <?include('blocks/sidebar-menu.php');?>
        </div>
        <div class="col-2">
            <div class="my-wardrobe">
                <div class="my-wardrobe__header">
                    <h1>Мой гардероб</h1>
                    <p>Для того, чтобы оформить возврат с помощью курьера, наведите на нужный товар и нажмите на кнопку «Вернуть», затем «Оформление возврата». <br>Так же, вы можете самостоятельно вернуть не подошедший вам товар в любом фирменном пункте самовывоза Wildberries или совершить возврат Почтой России.</p>
                </div>
                <div class="my-wardrobe__sub-heading">
                    <button class="btn continue-return-request">Продолжить оформление возврата</button>
                    <div class="sub-col search"><label><input type="text" placeholder="Найти"><i class="fas fa-search"></i></label></div>
                </div>
                <div class="my-wardrobe__top-nav">
                    <p>Заказов <span>3</span></p>
                    <ul class="catalog-products-pagination">
                        <li><a href=""><i class="fas fa-angle-left"></i></a></li>
                        <li class="catalog-products-pagination__item"><a href="">1</a></li>
                        <li class="active"><a href="">2</a></li>
                        <li><a href="">3</a></li>
                        <li><a href=""><i class="fas fa-angle-right"></i></a></li>
                    </ul>
                </div>
                <div class="my-wardrobe-list">
                    <ul class="my-wardrobe-list__list-heading">
                        <li>Товар</li>
                        <li>Номер заказа</li>
                        <li>Цена</li>
                        <li>Действие</li>
                    </ul>
                    <ul class="my-wardrobe-list__item-list">
                        <? for ($i=0; $i < 3; $i++) { ?>
                        <li>
                            <div class="item">
                                <div class="item__item-info column">
                                    <div class="item__image-container">
                                        <img src="/images/dynamic/product-image-1.jpg" alt="">
                                        <i class="item__flag-sale active">SALE</i>
                                    </div>
                                    <div class="item__info-container">
                                        <b>Рубашка</b>
                                        <b>Артикул: <span>2621383</span></b>
                                        <b>Цвет: <span>белый</span></b>
                                        <b>Размер: <span>38</span></b>
                                    </div>
                                </div>
                                <div class="column">24977875</div>
                                <div class="column">4570</div>
                                <div class="item__action column">
                                    <a class="item__action-leave-feedback" href=""><i class="fas fa-star"></i><span>Оставить отзыв</span></a>
                                    <div class="item__action-social">
                                        <a class="item__action-fb fab fa-facebook-f" href=""></a>
                                        <a class="item__action-ok fab fa-odnoklassniki" href=""></a>
                                        <a class="item__action-vk fab fa-vk" href=""></a>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <? } ?>
                    </ul>
                </div>
                <div class="my-wardrobe__bottom-nav">
                    <ul class="catalog-products-pagination">
                        <li><a href=""><i class="fas fa-angle-left"></i></a></li>
                        <li class="catalog-products-pagination__item"><a href="">1</a></li>
                        <li class="active"><a href="">2</a></li>
                        <li><a href="">3</a></li>
                        <li><a href=""><i class="fas fa-angle-right"></i></a></li>
                    </ul>
                    <button class="btn continue-return-request">Продолжить оформление возврата</button>
                </div>
            </div>
        </div>
    </div>
</div>