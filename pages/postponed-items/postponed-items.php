<div class="page page-postponed-items">
    <div class="page__inner page-postponed-items__inner">
        <div class="col-1">
        <?include('blocks/sidebar-menu.php');?>
        </div>
        <div class="col-2">
            <div class="postponed-items">
                <div class="postponed-items__header">
                    <div class="sub-col">
                        <h1>Отложенные товары</h1>
                        <p>Вы можете отложить не более 1000 товаров.</p>
                    </div>
                    <div class="sub-col search"><label><input type="text" placeholder="Найти"><i class="fas fa-search"></i></label></div>
                </div>
                <div class="postponed-items__sub-heading">
                    <input type="text" placeholder="Ваш промо код">
                    <button class="btn btn-check-promo" type="button">Проверить промокод</button>
                    <button class="btn btn-delete" type="button">Удалить</button>
                    <button class="btn btn-add-to-cart" type="button">Положить в корзину</button>
                </div>
                <div class="postponed-items__top-nav">
                    <p>Заказов <span>3</span></p>
                    <ul class="catalog-products-pagination">
                        <li><a href=""><i class="fas fa-angle-left"></i></a></li>
                        <li class="catalog-products-pagination__item"><a href="">1</a></li>
                        <li class="active"><a href="">2</a></li>
                        <li><a href="">3</a></li>
                        <li><a href=""><i class="fas fa-angle-right"></i></a></li>
                    </ul>
                </div>
                <div class="postponed-items-list">
                    <ul class="postponed-items-list__list-heading">
                        <li class="select-all-inputs js-btn-autocheck"><i class="indicator"></i>Выделить все</li>
                        <li>Товар</li>
                        <li>Цена руб.</li>
                        <li>Статус</li>
                        <li>Действие</li>
                    </ul>
                    <ul class="postponed-items-list__item-list">
                        <? for ($i=0; $i < 3; $i++) { ?>
                        <li>
                            <input class="js-input-autocheck" type="checkbox" name="move-to-archive" value="24977875" id="appeal-id-<?echo($i)?>">
                            <label class="js-label-autocheck" for="appeal-id-<?echo($i)?>">
                                <div class="item">
                                    <div class="item__checkbox-container column">
                                        <i></i>
                                    </div>
                                    <div class="item__item-info column">
                                        <div class="item__image-container">
                                            <img src="/images/dynamic/product-image-1.jpg" alt="">
                                            <i class="item__flag-sale active">SALE</i>
                                        </div>
                                        <div class="item__info-container">
                                            <b>Рубашка</b>
                                            <b>Артикул: <span>2621383</span></b>
                                            <b>Цвет: <span>белый</span></b>
                                            <b>Размер: <span>38</span></b>
                                        </div>
                                    </div>
                                    <div class="column">249</div>
                                    <div class="column">Есть на складе</div>
                                    <div class="item__action column">
                                        <button class="btn item__action-add-to-cart"></button>
                                        <button class="btn item__action-delete"></button>
                                        <a class="item__action-folder-icon" href=""></a>
                                        <a class="item__action-folder-text" href="">Общая папка</a>
                                        <div class="item__action-social">
                                            <a class="item__action-fb fab fa-facebook-f" href=""></a>
                                            <a class="item__action-ok fab fa-odnoklassniki" href=""></a>
                                            <a class="item__action-vk fab fa-vk" href=""></a>
                                        </div>
                                    </div>
                                </div>
                            </label>
                        </li>
                        <? } ?>
                    </ul>
                </div>
                <div class="postponed-items__bottom-nav">
                    <ul class="catalog-products-pagination">
                        <li><a href=""><i class="fas fa-angle-left"></i></a></li>
                        <li class="catalog-products-pagination__item"><a href="">1</a></li>
                        <li class="active"><a href="">2</a></li>
                        <li><a href="">3</a></li>
                        <li><a href=""><i class="fas fa-angle-right"></i></a></li>
                    </ul>
                    <button class="btn btn-delete" type="button">Удалить</button>
                    <button class="btn btn-add-to-cart" type="button">Положить в корзину</button>
                </div>
            </div>
        </div>
    </div>
</div>