<div class="page page-product-card">
    <div class="page__inner page-product-card__inner">
        <div class="col-1">
            <div class="card-item-left">
                <div class="card-item-slider">
                    <div class="card-item-slider__vertical-scroll">
                        <div class="card-item-slider__preview-images">
                            <img src="/images/dynamic/product-image-1.jpg" alt="">
                            <img src="/images/dynamic/product-image-2.jpg" alt="">
                            <img src="/images/dynamic/product-image-3.jpg" alt="">
                            <img src="/images/dynamic/product-image-1.jpg" alt="">
                            <img src="/images/dynamic/product-image-2.jpg" alt="">
                            <img src="/images/dynamic/product-image-3.jpg" alt="">
                        </div>
                    </div>
                    <div class="card-item-slider__big-preview">
                        <img src="/images/dynamic/product-image-1.jpg" alt="">
                    </div>
                </div>
                <div class="card-item-under-slider">
                    <div class="card-item-share">
                        <h3>Поделиться</h3>
                        <ul>
                            <li><a class="fas fa-at" href=""></a></li>
                            <li><a class="fab fa-facebook-f" href=""></a></li>
                            <li><a class="fab fa-odnoklassniki" href=""></a></li>
                            <li><a class="fab fa-vk" href=""></a></li>
                            <li><a class="fab fa-twitter" href=""></a></li>
                        </ul>
                    </div>
                    <button class="btn btn-show-modal-inaccurate-description">Нашли неточность в описании? Сообщите нам!</button>
                    <p class="card-item-monogram-reminder">На любой купленной в  нашем интернет-магазине <br>рубашке мы можем вышить монограмму</p>
                </div>
                <div class="card-item-recommended-with">
                    <h3>С этим товаром рекомендуют:</h3>
                    <div>
                        <?include('blocks/products-list-item.php');?>
                        <?include('blocks/products-list-item.php');?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-2">
            <div class="card-item-right">
                <div class="card-item-info">
                    <h3 class="card-item-info__item-name">Рубашка хлопковая</h3>
                    <!-- Разметка и стили рейтинга из страницы catalog.php -->
                    <div class="card-item-info__row-1">
                        <div class="products-slider-item__rating">
                            <div class="products-slider-item__rating-stars"></div>
                            <span class="products-slider-item__rating-score">(5)</span>
                        </div>
                        <p>Товар заказали: <span>19315 раз</span></p>
                    </div>
                    <div class="card-item-info__row-2">
                        <b>4200 руб.</b>
                        <b>4750 руб.</b>
                        <b>-25%</b>
                    </div>
                    <p class="card-item-info__promocode-discount">
                        <span>3900 руб.</span>
                        <b class="promocode-popup">
                            <b class="title">До окончания акции</b>
                            <b class="automatically-applied">Промокод будет автоматически применен в Корзине</b>
                        </b>
                        доп.скидка по промокоду 20%</p>
                    <div class="card-item-info__buttons">
                        <div class="counter-controls">
                            <button class="counter-controls__btn-decrease">-</button>
                            <i class="counter-controls__amount">01</i>
                            <button class="counter-controls__btn-increase">+</button>
                        </div>
                        <button class="btn btn-add-to-favorite"><i class="far fa-heart"></i></button>
                        <button class="btn btn-add-to-cart">Добавить в корзину</button>
                    </div>
                    <div class="card-item-info__specifics">
                        <p>Артикул: <span class="card-item-info__itemcode" id="card-item-info__itemcode">2621383</span></p>
                        <p>Цвет: <span class="card-item-info__itemcolor" id="card-item-info__itemcolor">синий</span></p>
                        <p>Состав: <span class="card-item-info__itemmaterial">хлопок 100%</span></p>
                    </div>
                    <form class="card-item-form" id="card-item-form">
                        <h3>Выберите цвет</h3>
                        <ul class="card-item-form__color-list">
                            <li><input class="item-color-input" id="input-item-color-white" type="radio" name="item-color" value="белый" data-color="белый" data-code="123white" checked><label class="radiobutton-label" for="input-item-color-white"><span class="radiobutton-indicator" style="background-image: url(/images/product-card/colors/01-white.jpg);"></span>Белый</label></li>
                            <li><input class="item-color-input" id="input-item-color-red" type="radio" name="item-color" value="красный" data-color="красный" data-code="456red"><label class="radiobutton-label" for="input-item-color-red"><span class="radiobutton-indicator" style="background-image: url(/images/product-card/colors/02-red.jpg);"></span>Красный</label></li>
                            <li><input class="item-color-input" id="input-item-color-green" type="radio" name="item-color" value="зеленый" data-color="зеленый" data-code="789green"><label class="radiobutton-label" for="input-item-color-green"><span class="radiobutton-indicator" style="background-image: url(/images/product-card/colors/03-green.jpg);"></span>Зеленый</label></li>
                        </ul>
                    </form>
                    <div class="card-item-info__choose-size">
                        <h3>Выберите размер</h3>
                        <ul>
                            <!-- Здесь нужно div с modal писать на одной строке с </label> чтобы не появлялся whitespace -->
                            <?for ($i=37; $i <=44 ; $i++) { ?>
                                <li><label><input class="item-size-input" type="radio" name="item-size" value="<?echo($i)?>" form="card-item-form"><span><?echo($i)?></span></label><div class="modal modal-delivery-details">
                                        <div class="modal_inner modal-delivery-details__inner">
                                            <div class="col">
                                                <p>Ближайшая дата доставки: <span>09.03.2018</span></p>
                                                <p>Российский размер: <span>52</span></p>
                                                <p>Обхват талии в см: <span>86-89</span></p>
                                                <p>Обхват бедер в см: <span>104-107</span></p>
                                                <p>Рост см: <span>182</span></p>
                                                <p>Длина внутренней стороны ноги в см: <span>85-88</span></p>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            <? } ?>
                        </ul>
                        <div class="modal modal-delivery-details">
                            <div class="modal_inner modal-delivery-details__inner">
                                <div class="col">
                                    <p>Ближайшая дата доставки: <span>09.03.2018</span></p>
                                    <p>Российский размер: <span>52</span></p>
                                    <p>Обхват талии в см: <span>86-89</span></p>
                                    <p>Обхват бедер в см: <span>104-107</span></p>
                                    <p>Рост см: <span>182</span></p>
                                    <p>Длина внутренней стороны ноги в см: <span>85-88</span></p>
                                </div>
                            </div>
                        </div>

                        <button class="btn btn-show-modal-find-your-size">Определите свой размер</button>
                        <button class="btn btn-show-modal-found-cheaper">Нашли дешевле? Сообщите нам!</button>
                    </div>
                    <div class="card-item-info__choose-development">
                        <p>Выберите между разработкой своего уникального продукта или добавьте в корзину продукт созданный нами.</p>
                        <button class="btn">Нанести монограмму</button>
                        <button class="btn">Создать рубашку</button>
                    </div>
                </div>
                <div class="card-item-delivery-terms">
                    <h3 class="card-item-delivery-terms__header">Условия доставки</h3>
                    <p>Ваш населенный пункт:</p>
                    <b>Павловский Посад</b>
                    <p>Способ доставки:</p>
                    <b>Курьер, самовывоз</b>
                    <p>Ближайшая дата доставки:</p>
                    <b>09.03.2018</b>
                    <p>Стоимость доставки:</p>
                    <b>БЕСПЛАТНО</b>
                </div>
                <div class="card-item-parameters">
                    <h3>Параметры</h3>
                    <ul>
                        <li>Вид застежки: <span>Пуговицы</span></li>
                        <li>Длина рукава: <span>Длинные</span></li>
                        <li>Фактура материала: <span>текстильный</span></li>
                        <li>Тип карманов: <span>без карманов; в шве</span></li>
                        <li>Декоративные элементы: <span>без элементов</span></li>
                        <li>Уход за вещами: <span> бережная стирка при 30 градусах</span></li>
                        <li>Посадка рубашки: <span>приталенная</span></li>
                        <li>Длина изделия по спинке: <span>76 см</span></li>
                        <li>Сезон: <span>круглогодичный</span></li>
                        <li>Пол: <span>Мужской</span></li>
                        <li>Страна бренда: <span>Россия</span></li>
                        <li>Страна производитель: <span>Китай</span></li>
                        <li>Комплектация: <span>рубашка</span></li>
                    </ul>
                    <p>*Числовые параметры соответствуют размеру 37</p>
                </div>
                <div class="card-item-note">
                    <h3>Примечание</h3>
                    <p>Товар маломерит на 2 размера</p>
                    <p>Информация о технических характеристиках, комплекте поставки, стране изготовления и внешнем виде товара носит справочный характер и основывается на последних доступных сведениях от производителя.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="page-line slider-top">
        <div class="page-line__inner slider-top__inner">
            <div class="card-products-slider">
                <div class="card-products-slider__heading">
                    <span>Похожие товары</span>
                </div>
                <div class="card-products-slider__block products-slider">
                    <?for ($i=0; $i < 20; $i++) { 
                        include('blocks/products-list-item.php');
                    }?>
                </div>
            </div>
        </div>
    </div>

    <div class="page-line slider-bottom">
        <div class="page-line__inner slider-bottom__inner">
            <div class="card-products-slider">
                <div class="card-products-slider__heading">
                    <span>С этим товаром также заказывают</span>
                </div>
                <div class="card-products-slider__block products-slider">
                    <?for ($i=0; $i < 20; $i++) { 
                        include('blocks/products-list-item.php');
                    }?>
                </div>
            </div>
        </div>
    </div>

    <div class="page__inner page-product-card__inner">
        <div class="col-communication">
            <div class="card-communication">
                <div class="card-communication__header">
                    <h3 class="card-communication__item-name">Рубашка хлопковая</h3>
                    <div class="card-communication__header-btns">
                        <button class="btn tabs-btn">Отзывы <span>(32)</span></button>
                        <button class="btn tabs-btn">Вопросы <span>(3)</span></button>
                    </div>
                    <button class="btn btn-rules">Правила оформления отзывов и вопросов</button>
                </div>
                <div class="card-communication-tabs-container tabs">
                    <div class="card-communication-comments tabs-block">
                        <form class="card-communication-comments-form" name="comments-form">
                            <input type="submit" value="Написать отзыв">
                            <div class="card-communication-comments-form__rating-container">
                                <b>Оценка</b>
                                <div class="products-slider-item__rating">
                                    <div class="products-slider-item__rating-stars"></div>
                                    <span class="products-slider-item__rating-score">(5)</span>
                                </div>
                            </div>
                            <div class="card-communication-comments-form__main-input">
                                <div class="card-communication-comments-form__text-fields">
                                    <div>
                                        <label for="item-colors-select">Цвет</label>
                                        <label for="item-sizes-select">Размер</label>
                                    </div>
                                    <div class="input-container">
                                        <select name="item-color" id="item-colors-select">
                                            <option value="белый">белый</option>
                                            <option value="красный">красный</option>
                                            <option value="зеленый">зеленый</option>
                                        </select>
                                        <select name="item-size" id="item-sizes-select">
                                            <option value="37">37</option>
                                            <option value="38">38</option>
                                            <option value="39">39</option>
                                            <option value="40">40</option>
                                            <option value="41">41</option>
                                            <option value="42">42</option>
                                            <option value="43">43</option>
                                            <option value="44">44</option>
                                        </select>
                                    </div>
                                    <textarea cols="65" rows="10" name="user-textarea" placeholder="Оставьте сообщение"></textarea>
                                    <p class="textarea-symbols-left">Введено символов <span>0</span> / 1000</p>
                                </div>
                                <div class="card-communication-comments-form__item-congruity">
                                    <div class="heading-wrapper">
                                        <b>Соответствие размеру</b>
                                        <input type="reset" value="Выбрано по умолчанию">
                                    </div>
                                    <div class="input-container">
                                        <div><input type="radio" value="Меньше" name="size-congruity" id="size-less" checked><label for="size-less">Меньше</label></div>
                                        <div><input type="radio" value="Больше" name="size-congruity" id="size-more"><label for="size-more">Больше</label></div>
                                        <div><input type="radio" value="Полное" name="size-congruity" id="size-full"><label for="size-full">Полное</label></div>
                                    </div>
                                    <b>Соответствие фото</b>
                                    <div class="input-container">
                                        <div><input type="radio" value="Меньше" name="photo-congruity" id="photo-less" checked><label for="photo-less">Меньше</label></div>
                                        <div><input type="radio" value="Больше" name="photo-congruity" id="photo-more"><label for="photo-more">Больше</label></div>
                                        <div><input type="radio" value="Полное" name="photo-congruity" id="photo-full"><label for="photo-full">Полное</label></div>
                                    </div>
                                    <b>Соответствие описанию</b>
                                    <div class="input-container">
                                        <div><input type="radio" value="Меньше" name="description-congruity" id="description-less" checked><label for="description-less">Меньше</label></div>
                                        <div><input type="radio" value="Больше" name="description-congruity" id="description-more"><label for="description-more">Больше</label></div>
                                        <div><input type="radio" value="Полное" name="description-congruity" id="description-full"><label for="description-full">Полное</label></div>
                                    </div>
                                    <b>Видимость в профайле</b>
                                    <div class="input-container input-container__last">
                                        <select class="profile-visibility" value="Всем">
                                            <option value="Всем">Всем</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="card-communication-comments-form__file-input-container">
                                <label class="file-input-label">
                                    <input class="card-communication-comments-form__file-input" type="file" name="user-file-input">
                                </label>
                                <div class="file-input-description">
                                    <p>Вы можете загрузить 5 изображений. Размер каждого файла не должен превышать 2 МБ.</p>
                                    <p>Поддерживаемые форматы: JPG, JPEG, PNG</p>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-file-input-submit">Отправить</button>
                            <button type="reset" class="btn btn-file-input-cancel">Отменить</button>
                        </form>
                        <div class="card-communication-comments-filter">
                            <div class="card-communication-comments-filter__heading">
                                <b>Сортировать по:</b>
                                <button class="btn just-btn card-communication-comments-filter__sort-by active">дате</button>
                                <button class="btn card-communication-comments-filter__sort-by">оценке</button>
                                <button class="btn card-communication-comments-filter__sort-by">полезности</button>
                                <input class="photo-required" type="checkbox" id="photo-required">
                                <label class="btn card-communication-comments-filter__photo-required" for="photo-required"><span>с фото</span></label>
                            </div>
                            <div class="card-communication-comments-container">

                                <div class="card-communication-comments-post">
                                    <div class="card-communication-comments-post__profile">
                                        <img src="/images/static/avatar-placeholder.png" alt="">
                                        <b>Игорь</b>
                                    </div>
                                    <div class="card-communication-comments-post__main-field">
                                        <div class="card-communication-comments-post__top-field">
                                            <div class="top-left">
                                                <p>5 августа 2017, 1:32</p>
                                                <div>
                                                    <div class="products-slider-item__rating">
                                                        <div class="products-slider-item__rating-stars"></div>
                                                        <span class="products-slider-item__rating-score">(5)</span>
                                                    </div>
                                                    <b>Цвет: желтый</b><b>Размер: 38</b>
                                                </div>
                                            </div>
                                            <div class="bottom-right">
                                                <p>Отзыв полезен?</p>
                                                <button class="btn far fa-thumbs-up active"></button><span>4</span>
                                                <button class="btn far fa-thumbs-down"></button><span>0</span>
                                            </div>
                                        </div>
                                        <div class="card-communication-comments-post__middle-field">
                                            <p>41 размер хорошо сел на российский 48-50. На стройную фигуру без живота, зауженный фасон. Темно-синий цвет глубокий, красивый, смотрится очень стильно. Качество хорошее, приятная к телу. </p>
                                        </div>
                                        <div class="card-communication-comments-post__bottom-field">
                                            <div class="congruity-block">
                                                <b>Соответствие размеру</b>
                                                <span>Полное</span>
                                            </div>
                                            <div class="congruity-block">
                                                <b>Соответствие описанию</b>
                                                <span>Полное</span>
                                            </div>
                                            <div class="congruity-block">
                                                <b>Соответствие фото</b>
                                                <span>Полное</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <button class="btn btn-show-more-posts">Показать ещё отзывы</button>
                            </div>
                        </div>
                    </div>
                    <div class="card-communication-questions tabs-block">
                        <div class="card-communication-questions__heading">
                            <p>Чтобы узнать срок поступления товара, добавьте его в <a href="">Лист Ожидания</a>.
                                <br>Здесь Вы можете задать любой вопрос по товару. Ответ на вопрос будет предоставлен в течение трех рабочих дней.</p>
                            <p>Другие вопросы Вы можете задать в разделе <a href="">Мои обращения</a> в Личном кабинете или воспользовавшись способами связи из раздела <a href="">Контакты</a>.</p>
                        </div>
                        <form class="card-communication-questions-form">
                            <textarea name="questions-form-textarea" cols="90" rows="10" placeholder="Напишите ваш вопрос по товару"></textarea>
                            <p class="textarea-symbols-right">Введено символов <span>0</span> / 1000</p>
                            <button type="submit" class="btn btn-file-input-submit">Отправить</button>
                            <button type="reset" class="btn btn-file-input-cancel">Отменить</button>
                        </form>
                        <div class="card-communication-questions-container">
                            <div class="card-communication-questions-post">
                                <div class="card-communication-questions-post__profile">
                                    <img src="/images/static/avatar-placeholder.png" alt="">
                                    <b>Игорь</b>
                                </div>
                                <div class="card-communication-questions-post__main-field">
                                    <div class="card-communication-questions-post__top-field">
                                        <p>5 августа 2017, 1:32</p>
                                    </div>
                                    <div class="card-communication-questions-post__middle-field">
                                        <p>Здравствуйте, какой размер рубашки, если размер m?</p>
                                    </div>
                                    <div class="card-communication-questions-post__bottom-field">
                                        <div class="card-communication-questions-post__reply-top">
                                            <b class="sub-row sub-row__responder">Представитель магазина</b>
                                            <b class="sub-row sub-row__date">5 августа 2017, 1:32</b>
                                            <b class="sub-row sub-row__rate-answer">Оцените ответ</b>
                                            <div class="products-slider-item__rating">
                                                <div class="products-slider-item__rating-stars"></div>
                                                <span class="products-slider-item__rating-score">(5)</span>
                                            </div>
                                        </div>
                                        <div class="card-communication-questions-post__reply-bottom">
                                            <p>Здравствуйте. 40</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-communication-questions-post">
                                <div class="card-communication-questions-post__profile">
                                    <img src="/images/static/avatar-placeholder.png" alt="">
                                    <b>Игорь</b>
                                </div>
                                <div class="card-communication-questions-post__main-field">
                                    <div class="card-communication-questions-post__top-field">
                                        <p>5 августа 2017, 1:32</p>
                                    </div>
                                    <div class="card-communication-questions-post__middle-field">
                                        <p>Здравствуйте, какой размер рубашки, если размер m?</p>
                                    </div>
                                    <div class="card-communication-questions-post__bottom-field">
                                        <div class="card-communication-questions-post__reply-top">
                                            <b class="sub-row sub-row__responder">Представитель магазина</b>
                                            <b class="sub-row sub-row__date">5 августа 2017, 1:32</b>
                                            <b class="sub-row sub-row__rate-answer">Оцените ответ</b>
                                            <div class="products-slider-item__rating">
                                                <div class="products-slider-item__rating-stars"></div>
                                                <span class="products-slider-item__rating-score">(5)</span>
                                            </div>
                                        </div>
                                        <div class="card-communication-questions-post__reply-bottom">
                                            <p>Здравствуйте. 40</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-communication-questions-post">
                                <div class="card-communication-questions-post__profile">
                                    <img src="/images/static/avatar-placeholder.png" alt="">
                                    <b>Игорь</b>
                                </div>
                                <div class="card-communication-questions-post__main-field">
                                    <div class="card-communication-questions-post__top-field">
                                        <p>5 августа 2017, 1:32</p>
                                    </div>
                                    <div class="card-communication-questions-post__middle-field">
                                        <p>Здравствуйте, какой размер рубашки, если размер m?</p>
                                    </div>
                                    <div class="card-communication-questions-post__bottom-field">
                                        <div class="card-communication-questions-post__reply-top">
                                            <b class="sub-row sub-row__responder">Представитель магазина</b>
                                            <b class="sub-row sub-row__date">5 августа 2017, 1:32</b>
                                            <b class="sub-row sub-row__rate-answer">Оцените ответ</b>
                                            <div class="products-slider-item__rating">
                                                <div class="products-slider-item__rating-stars"></div>
                                                <span class="products-slider-item__rating-score">(5)</span>
                                            </div>
                                        </div>
                                        <div class="card-communication-questions-post__reply-bottom">
                                            <p>Здравствуйте. 40</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>