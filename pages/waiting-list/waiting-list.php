<div class="page page-waiting-list">
    <div class="page__inner page-waiting-list__inner">
        <div class="col-1">
            <?include('blocks/sidebar-menu.php');?>
        </div>
        <div class="col-2">
            <div class="waiting-list">                
            <h1>Лист-ожидания</h1>
                <div class="waiting-list__header">
                    <button class="btn btn-delete" type="button">Удалить</button>
                    <button class="btn btn-add-to-cart" type="button">Положить в корзину</button>
                    <div class="sub-col search"><label><input type="text" placeholder="Найти"><i class="fas fa-search"></i></label></div>
                </div>
                <div class="waiting-list__sub-heading">
                    <p>Вы добавили в свой Лист ожидания товары, которые на данный момент отсутствуют в продаже. Если товар есть у поставщика и не снят с производства, мы обязательно сделаем заказ. Как только товар поступит в продажу, эта информация отобразится в данном разделе.</p>
                    <p>Вы можете добавить в Лист ожидания не более 1000 товаров.</p>
                    <p>Обратите внимание, что обновление информации в Листе ожидания о наличии товара может происходить с некоторой задержкой.</p>
                    <button class="notify-me">
                        <span>Оповещать меня о появлении товаров в продаже</span>
                        <b class="hover-popup">
                            <b>Получайте сообщение по email, как только товар из Вашего Листа ожидания появился в продаже!</b>
                        </b>
                    </button>
                    
                </div>
                <div class="waiting-list__top-nav">
                    <p>Заказов <span>3</span></p>
                    <ul class="catalog-products-pagination">
                        <li><a href=""><i class="fas fa-angle-left"></i></a></li>
                        <li class="catalog-products-pagination__item"><a href="">1</a></li>
                        <li class="active"><a href="">2</a></li>
                        <li><a href="">3</a></li>
                        <li><a href=""><i class="fas fa-angle-right"></i></a></li>
                    </ul>
                </div>
                <div class="waiting-list-list">
                    <ul class="waiting-list-list__list-heading">
                        <li class="select-all-inputs js-btn-autocheck"><i class="indicator"></i>Выделить все</li>
                        <li>Товар</li>
                        <li>Цена руб.</li>
                        <li>Статус</li>
                        <li>Действие</li>
                    </ul>
                    <ul class="waiting-list-list__item-list">
                        <? for ($i=0; $i < 3; $i++) { ?>
                        <li>
                            <input class="js-input-autocheck" type="checkbox" name="move-to-archive" value="24977875" id="appeal-id-<?echo($i)?>">
                            <label class="js-label-autocheck" for="appeal-id-<?echo($i)?>">
                                <div class="item">
                                    <div class="item__checkbox-container column">
                                        <i></i>
                                    </div>
                                    <div class="item__item-info column">
                                        <div class="item__image-container">
                                            <img src="/images/dynamic/product-image-1.jpg" alt="">
                                            <i class="item__flag-sale active">SALE</i>
                                        </div>
                                        <div class="item__info-container">
                                            <b>Рубашка</b>
                                            <b>Артикул: <span>2621383</span></b>
                                            <b>Цвет: <span>белый</span></b>
                                            <b>Размер: <span>38</span></b>
                                        </div>
                                    </div>
                                    <div class="column">249</div>
                                    <div class="column">В ближайшее время поставок не ожидается</div>
                                    <div class="item__action column">
                                        <button class="btn item__action-delete"></button>
                                        <div class="item__action-social">
                                            <a class="item__action-fb fab fa-facebook-f" href=""></a>
                                            <a class="item__action-ok fab fa-odnoklassniki" href=""></a>
                                            <a class="item__action-vk fab fa-vk" href=""></a>
                                        </div>
                                    </div>
                                </div>
                            </label>
                        </li>
                        <? } ?>
                    </ul>
                </div>
                <div class="waiting-list__bottom-nav">
                    <ul class="catalog-products-pagination">
                        <li><a href=""><i class="fas fa-angle-left"></i></a></li>
                        <li class="catalog-products-pagination__item"><a href="">1</a></li>
                        <li class="active"><a href="">2</a></li>
                        <li><a href="">3</a></li>
                        <li><a href=""><i class="fas fa-angle-right"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>