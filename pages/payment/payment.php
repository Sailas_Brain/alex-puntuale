<div class="page page-payment">
    <div class="page__inner page-payment__inner">
        <div class="col-1">
            <?include('blocks/sidebar-menu.php');?>
        </div>
        <div class="col-2">
            <div class="payment">
                <h1>Оформление заказа</h1>
                <div class="payment__steps">
                    <span class="payment__step"><i></i>Корзина</span>
                    <span class="payment__step"><i></i>Доставка</span>
                    <span class="payment__step active"><i></i>Оплата</span>
                </div>
                <div class="payment-methods">
                    <div class="payment-methods__col-1">
                        <p class="title">Выберите способ оплаты</p>
                        <form action="">
                        <div class="payment-methods__method">
                            <input class="tabs-btn" id="method-1" type="radio" name="payment-method" checked>
                            <label for="method-1">
                                <span>Оплата при получении</span>
                            </label>
                        </div>
                        <div class="payment-methods__method">
                            <input class="tabs-btn" id="method-2" type="radio" name="payment-method">
                            <label for="method-2">
                                <span>Мой баланс</span>
                            </label>
                        </div>
                        <div class="payment-methods__method">
                            <input class="tabs-btn" id="method-3" type="radio" name="payment-method">
                            <label for="method-3">
                                <span>Онлайн оплата картой</span>
                            </label>
                        </div>
                        <div class="payment-methods__method">
                            <input class="tabs-btn" id="method-4" type="radio" name="payment-method">
                            <label for="method-4">
                                <span>Яндекс Деньги</span>
                            </label>
                        </div>
                        <div class="payment-methods__method">
                            <input class="tabs-btn" id="method-5" type="radio" name="payment-method">
                            <label for="method-5">
                                <span>WebMoney</span>
                            </label>
                        </div>
                        <div class="payment-methods__method">
                            <input class="tabs-btn" id="method-6" type="radio" name="payment-method">
                            <label for="method-6">
                                <span>Paypal</span>
                            </label>
                        </div>
                        </form>
                    </div>
                    <div class="payment-methods__col-2">
                        <div class="payment-methods__card-info tabs-block">
                            <p>Вы можете оплатить свой заказ наличными при доставке курьером и в пунктах самовывоза.</p>
                            <p>Также Вы можете произвести оплату банковской картой курьеру с помощью специальных переносных мобильных терминалов.</p>
                            <p>Мы принимаем платежи по следующим банковским картам:</p>
                            <i class="visa"></i><i class="maestro"></i><i class="mastercard"></i>
                            <p>Мы не принимаем к оплате банковские карты: American Express, STB, Cirrus, Diners Club International.</p>
                        </div>
                        <div class="payment-methods__card-info tabs-block">
                            <p class="warning">Заказ будет оформлен на доставку после поступления оплаты</p>
                            <p>Оплата по электронным кошелькам возможна через системы электронных платежей..</p>
                            <p class="warning warning--icon">Для получения предоплаченного заказа Вам необходимо иметь при себе документ удостоверяющий личность (паспорт или водительские права).</p>
                        </div>
                        <div class="payment-methods__card-info tabs-block">
                            <p>Вы можете оплатить свой заказ наличными при доставке курьером и в пунктах самовывоза.</p>
                            <p>Также Вы можете произвести оплату банковской картой курьеру с помощью специальных переносных мобильных терминалов.</p>
                            <p>Мы принимаем платежи по следующим банковским картам:</p>
                            <i class="visa"></i><i class="maestro"></i><i class="mastercard"></i>
                            <p>Мы не принимаем к оплате банковские карты: American Express, STB, Cirrus, Diners Club International.</p>
                        </div>
                        <div class="payment-methods__card-info tabs-block">
                            <p class="warning">Заказ будет оформлен на доставку после поступления оплаты</p>
                            <p>Оплата по электронным кошелькам возможна через системы электронных платежей..</p>
                            <p class="warning warning--icon">Для получения предоплаченного заказа Вам необходимо иметь при себе документ удостоверяющий личность (паспорт или водительские права).</p>
                        </div>
                        <div class="payment-methods__card-info tabs-block">
                            <p>Вы можете оплатить свой заказ наличными при доставке курьером и в пунктах самовывоза.</p>
                            <p>Также Вы можете произвести оплату банковской картой курьеру с помощью специальных переносных мобильных терминалов.</p>
                            <p>Мы принимаем платежи по следующим банковским картам:</p>
                            <i class="visa"></i><i class="maestro"></i><i class="mastercard"></i>
                            <p>Мы не принимаем к оплате банковские карты: American Express, STB, Cirrus, Diners Club International.</p>
                        </div>
                        <div class="payment-methods__card-info tabs-block">
                            <p class="warning">Заказ будет оформлен на доставку после поступления оплаты</p>
                            <p>Оплата по электронным кошелькам возможна через системы электронных платежей..</p>
                            <p class="warning warning--icon">Для получения предоплаченного заказа Вам необходимо иметь при себе документ удостоверяющий личность (паспорт или водительские права).</p>
                        </div>
                        <div class="payment-methods__more-info tabs-block">
                            <b>Подробнее о <a href="">Способах оплаты</a></b>
                            <p>Безопасность онлайн-платежей обеспечивается использованием SSL протокола для передачи конфиденциальной информации от клиента на сервер системы для дальнейшей обработки.</p>
                        </div>
                    </div>
                </div>
                <div class="payment__summary summary">
                    <div class="summary__summ">
                        <p>Сумма заказа</p>
                        <span class="previous-price">1 376руб</span>
                        <span class="price">759 руб</span>
                    </div>
                    <div class="summary__total">
                        <p>Итого</p>
                        <span class="price">759 руб</span>
                    </div>
                    <div class="summary__discount">
                        <p>Вы экономите</p>
                        <span class="price">659 руб</span>
                    </div>
                    <div class="summary__btn-wrapper">
                        <a class="summary__btn-back" href="">Назад</a>
                        <a class="summary__btn-continue" href="">Продолжить</a>
                    </div>
                </div>
            </div>
            <section class="payment-list">
                <ul class="payment-list__list-heading">
                    <li>Описание</li>
                    <li>Цена</li>
                    <li>Промокод</li>
                    <li>Сумма</li>
                </ul>
                <div class="payment-list__item-list">
                    <div class="item">
                        <div class="item__item-info column">
                            <div class="item__image-container">
                                <img src="/images/dynamic/product-image-1.jpg" alt="">
                                <i class="item__flag-sale active">SALE</i>
                            </div>
                            <div class="item__info-container">
                                <b>Рубашка</b>
                                <b>Артикул: <span>2621383</span></b>
                                <b>Цвет: <span>белый</span></b>
                                <b>Размер: <span>38</span></b>
                                <div class="item-amount-controls">
                                    <button class="item-amount-controls__btn-decrease">-</button>
                                    <i class="item-amount-controls__amount">01</i>
                                    <button class="item-amount-controls__btn-increase">+</button>
                                </div>
                            </div>
                        </div>
                        <div class="column flex">
                            <b>-25%</b>
                            <b>4750 руб.</b>
                            <b>4200 руб.</b>
                        </div>
                        <div class="column flex">
                            <b>-20%</b>
                            <b>949 руб.</b>
                            <b>759 руб.</b>
                            <i class="item__promocode">CFFHJ87</i>
                        </div>
                        <div class="column">
                            <b>759 руб.</b>
                        </div>
                        <div class="item__action column">
                            <a class="btn-delete" href=""><i></i></a>
                            <a class="btn-favorite" href=""><i></i></a>
                            <a class="btn-edit" href=""><i></i></a>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>