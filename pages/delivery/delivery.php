<div class="page page-delivery">
	<div class="page-line delivery-forming">
		<div class="page-line__inner delivery-forming__inner">
			<div class="col-1">
				<?include('blocks/sidebar-menu.php');?>
			</div>
			<div class="col-2">
				<form method="post">
					<h2 class="delivery-forming__header">Оформление заказа</h2>
					<div class="delivery-forming__steps">
						<span class="delivery-forming__step"><i></i>Корзина</span>
						<span class="delivery-forming__step active"><i></i>Доставка</span>
						<span class="delivery-forming__step"><i></i>Оплата</span>
					</div>
					<div class="delivery-method tabs">
						<b class="delivery-method__choose-method">Выберите способ доставки</b>
						<p class="warning">После оформления заказа смена способа доставки невозможна</p>
						<div class="delivery-method__method-list">
							<div class="delivery-method__method-name tabs-btn"><span>Самовывоз</span></div>
							<div class="delivery-method__method-name tabs-btn active"><span>Доставка Курьером</span></div>
							<div class="delivery-method__method-name tabs-btn"><span>Постамат</span></div>
						</div>
						<div class="tabs">
							<div class="delivery-method__method method-pickup tabs-block">
								<b class="delivery-method__method-title">Забрать заказ самостоятельно</b>
								<button type="button" class="btn editable-list__btn-add for-modal">Добавить пункт самовывоза</button>
								<p class="simple-par">В пунктах самовывоза возможна наличная и безналичная оплата.<br>
								Во всех пунктах Вы можете воспользоваться удобными примерочными. Если товар не подошёл, Вы можете сразу вернуть его менеджеру пункта самовывоза.</p>
							</div>
							<div class="delivery-method__method method-courier tabs-block active">
								<b class="delivery-method__method-title">Заказать доставку курьером по адресу</b>
								<p class="simple-par">В случае указания полного адреса (этаж, подъезд, домофон или код), курьеру не нужно будет уточнять его дополнительно.</p>
								<ul class="delivery-method__editable-list editable-list">
									<li><label class="radiobutton-label"><input type="radio" name="address" value="Москва, Расковой, д. 26, кв. 31" checked><span class="radiobutton-indicator"></span>Москва, Расковой, д. 26, кв. 31<button class="editable-list__btn-edit"><i></i></button><button class="editable-list__btn-delete"><i></i></button></label></li>
								</ul>
								<button class="btn editable-list__btn-add">Добавить адрес</button>
								<p class="simple-par special-par">Проверка и примерка заказанного товара, как и все взаиморасчеты с курьером, производястя не более, чем за 20 минут.<br>Если товар не подошёл, вы можете сразу вернуть его курьеру.</p>
								<b class="delivery-method__choose-date">Выберите удобную для вас дату доставки</b>
								<div class="delivery-method__choose-date-input-wrapper">
									<label>
										<input class="delivery-method__choose-date-input" type="text" name="choose-delivery-date" id="datepicker" placeholder="Укажите интервал доставки">
										<i class="delivery-method__choose-date-input-icon"></i>
									</label>
								</div>
							</div>
							<div class="tabs-block"></div>
						</div>
						<div class="delivery-method__method method-postamat">

						</div>
					</div>
					<p class="delivery-forming__another-reciever">Получение заказа другим человеком</p>
					<p class="warning">Важно: ответственность за точность информации несет владелец Личного кабинета</p>
					<p class="simple-par">Если ваш заказ получает другой человек, введите его данные ниже:</p>
					<ul class="delivery-method__editable-list editable-list">
						<li><label class="radiobutton-label"><input type="radio" name="reciever" value="Дымова Лариса Сергеевна, +7 (926) 220-63-86" checked><span class="radiobutton-indicator"></span>Дымова Лариса Сергеевна, +7 (926) 220-63-86<button class="editable-list__btn-edit"><i></i></button><button class="editable-list__btn-delete"><i></i></button></label></li>
						</ul>
					<button type="button" class="btn editable-list__btn-add btn__add-reciever">Добавить получателя</button>
					<label class="checkbox-label inform-user"><input type="checkbox" name="inform-user" id="inform-user" value="yes" checked><span class="checkbox-indicator"></span>Информировать получателя по СМС об изменении статуса заказа</label>
					<div class="delivery-method__summary summary">
						<div class="summary__summ">
							<p>Сумма заказа</p>
							<span class="previous-price">1 376руб</span>
							<span class="price">759 руб</span>
						</div>
						<div class="summary__total">
							<p>Итого</p>
							<span class="price">759 руб</span>
						</div>
						<div class="summary__discount">
							<p>Вы экономите</p>
							<span class="price">659 руб</span>
						</div>
						<div class="summary__btn-wrapper">
							<a class="summary__btn-back" href="">Назад</a>
							<a class="summary__btn-continue" href="">Продолжить</a>
						</div>
					</div>
				</form>
                <section class="delivery-list">
                    <ul class="delivery-list__list-heading">
                        <li>Описание</li>
                        <li>Цена</li>
                        <li>Промокод</li>
                        <li>Сумма</li>
                    </ul>
                    <div class="delivery-list__item-list">
                        <? for ($i=0; $i < 1 ; $i++) { ?>
                        <div class="item">
                            <div class="item__item-info column">
                                <div class="item__image-container">
                                    <img src="/images/dynamic/product-image-1.jpg" alt="">
                                    <i class="item__flag-sale active">SALE</i>
                                </div>
                                <div class="item__info-container">
                                    <b>Рубашка</b>
                                    <b>Артикул: <span>2621383</span></b>
                                    <b>Цвет: <span>белый</span></b>
                                    <b>Размер: <span>38</span></b>
                                    <div class="item-amount-controls">
                                        <button class="item-amount-controls__btn-decrease">-</button>
                                        <i class="item-amount-controls__amount">01</i>
                                        <button class="item-amount-controls__btn-increase">+</button>
                                    </div>
                                </div>
                            </div>
                            <div class="column flex">
                                <b>-25%</b>
                                <b>4750 руб.</b>
                                <b>4200 руб.</b>
                            </div>
                            <div class="column flex">
                                <b>-20%</b>
                                <b>949 руб.</b>
                                <b>759 руб.</b>
                                <i class="item__promocode">CFFHJ87</i>
                            </div>
                            <div class="column">
                                <b>759 руб.</b>
                            </div>
                            <div class="item__action column">
                                <a class="btn-delete" href=""><i></i></a>
                                <a class="btn-favorite" href=""><i></i></a>
                                <a class="btn-edit" href=""><i></i></a>
                            </div>
                        </div>
                        <? } ?>
                    </div>
                </section>
			</div>
		</div>
	</div>
</div>