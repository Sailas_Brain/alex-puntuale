<div class="page page-cart">
    <div class="page__inner page-cart__inner">
        <div class="col-1">
            <?include('blocks/sidebar-menu.php');?>
        </div>
        <div class="col-2">
            <div class="cart">
                <h1>Оформление заказа</h1>
                <div class="cart__steps">
                    <span class="cart__step active"><i></i>Корзина</span>
                    <span class="cart__step"><i></i>Доставка</span>
                    <span class="cart__step"><i></i>Оплата</span>
                    <span class="cart__step">Скидка постоянного покупателя <a href="">подробнее</a><i>5%</i></span>
                </div>
                <section class="cart-list">
                    <ul class="cart-list__list-heading">
                        <li>Описание</li>
                        <li>Цена</li>
                        <li>Промокод</li>
                        <li>Сумма</li>
                    </ul>
                    <div class="cart-list__item-list">
                        <? for ($i=0; $i < 2 ; $i++) { ?>
                        <div class="item">
                            <div class="item__item-info column">
                                <div class="item__image-container">
                                    <img src="/images/dynamic/product-image-1.jpg" alt="">
                                    <i class="item__flag-sale active">SALE</i>
                                </div>
                                <div class="item__info-container">
                                    <b>Рубашка</b>
                                    <b>Артикул: <span>2621383</span></b>
                                    <b>Цвет: <span>белый</span></b>
                                    <b>Размер: <span>38</span></b>
                                    <div class="item-amount-controls">
                                        <button class="item-amount-controls__btn-decrease">-</button>
                                        <i class="item-amount-controls__amount">01</i>
                                        <button class="item-amount-controls__btn-increase">+</button>
                                    </div>
                                </div>
                            </div>
                            <div class="column flex">
                                <b>-25%</b>
                                <b>4750 руб.</b>
                                <b>4200 руб.</b>
                            </div>
                            <div class="column flex">
                                <b>-20%</b>
                                <b>949 руб.</b>
                                <b>759 руб.</b>
                                <i class="item__promocode">CFFHJ87</i>
                            </div>
                            <div class="column">
                                <b>759 руб.</b>
                            </div>
                            <div class="item__action column">
                                <a class="btn-delete" href=""><i></i></a>
                                <a class="btn-favorite" href=""><i></i></a>
                                <a class="btn-edit" href=""><i></i></a>
                            </div>
                        </div>
                        <? } ?>
                    </div>
                </section>
                <div class="enter-promocode">
                    <h2>Ввести промокод</h2>
                    <input type="text" placeholder="Введите свой промо код">
                    <button>Применить</button>
                </div>
                <div class="payment__summary summary">
                    <div class="summary__summ">
                        <p>Сумма заказа</p>
                        <span class="previous-price">1 376руб</span>
                        <span class="price">759 руб</span>
                    </div>
                    <div class="summary__total">
                        <p>Итого</p>
                        <span class="price">759 руб</span>
                    </div>
                    <div class="summary__discount">
                        <p>Вы экономите</p>
                        <span class="price">659 руб</span>
                    </div>
                    <div class="summary__btn-wrapper">
                        <a class="summary__btn-back" href="">Назад</a>
                        <a class="summary__btn-continue" href="">Продолжить</a>
                    </div>
                </div>
                <div class="one-click-order">
                    <p class="one-click-order__heading">Мы автоматически подобрали последний выбранный адрес и способ оплаты. Вы можете оформить заказ в 1 клик без перехода на следующие шаги корзины.</p>
                    <div class="one-click-order__info">
                        <div class="one-click-order__column">
                            <h3>Доставка</h3>
                            <b>Курьер</b>
                            <p class="address">Москва, Расковой, д. 26, кв. 31</p>
                            <p>Дата доставки: <span>12.03.2018</span></p>
                            <p>Время доставки: <span>15:00-18:00</span></p>
                        </div>
                        <div class="one-click-order__column">
                            <h3>Способ оплаты</h3>
                            <b>Оплата при получении</b>
                            <p>Товаров: <span>1 шт.</span></p>
                            <p>Сумма: <span>759 руб.</span></p>
                        </div>
                    </div>
                    <a class="btn-one-click-order" href="">Оформить в 1 клик <b class="warning-popup">Внимание! Ваш заказ будет сразу оформлен на доставку по указанному адресу</b></a>
                </div>
            </div>
        </div>
    </div>
</div>z