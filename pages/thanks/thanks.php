<div class="page page-thanks">
    <div class="page__inner page-thanks__inner">
        <div class="col-1">
            <div class="thanks">
                <p class="thanks__header">Кристина,</p>
                <p class="thanks__header">спасибо, что выбрали магазин Puntiale!</p>
                <p class="thanks__order-confirmed">Ваш заказ подтверждён!</p>
                <p class="thanks__order-number">Номер заказа 895674890</p>
                <div class="thanks__cost-calculation">
                    <p class="thanks__delivery-cost">Бесплатная доставка</p>
                    <p class="thanks__total-cost">Итого: 2460 руб.</p>
                </div>
                <p class="thanks__tracking-info">Вы моежете следить за изменениями статуса заказа в <a href="">личном кабинете</a></p>
                <p class="thanks__call-us-back">Мы с радаостью ответим на все ваши вопросы: <span class="thanks__phone-number">8 800 800 80 80</span></p>
            </div>
        </div>
    </div>
</div>