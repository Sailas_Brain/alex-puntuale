<div class="page page-my-orders">
    <div class="page__inner page-my-orders__inner">
        <div class="col-1">
            <?include('blocks/sidebar-menu.php');?>
        </div>
        <div class="col-2">
            <h1>Мои заказы</h1>
            <div class="my-orders tabs">
                <div class="my-orders__header-list">
                    <div class="delivery-method__method-name tabs-btn active"><span>Мои заказы</span></div>
                    <div class="delivery-method__method-name tabs-btn"><span>Доставки</span></div>
                    <div class="delivery-method__method-name tabs-btn"><span>Оценка качества обслуживания</span></div>
                </div>
                <div>
                    <div class="my-orders-tab tabs-block">
                        <ul class="general-links">
                            <li><a href="">Все заказы</a></li>
                            <li><a href="">Цифровой контент</a></li>
                            <li><a href="">Подарочные сертификаты</a></li>
                        </ul>
                        <div class="my-orders-tab__top-nav">
                            <p>Заказов <span>31</span></p>
                            <ul class="catalog-products-pagination">
                                <li><a href=""><i class="fas fa-angle-left"></i></a></li>
                                <li class="catalog-products-pagination__item"><a href="">1</a></li>
                                <li class="active"><a href="">2</a></li>
                                <li><a href="">3</a></li>
                                <li><a href=""><i class="fas fa-angle-right"></i></a></li>
                            </ul>
                        </div>
                        <div class="my-orders-list">
                            <ul class="my-orders-list__list-heading">
                                <li>Заказ</li>
                                <li>Дата и время заказа</li>
                                <li>Статус</li>
                                <li>Сумма</li>
                                <li>Способ оплаты</li>
                            </ul>
                            <? for ($i=0; $i < 4; $i++) { ?>
                                <div class="my-orders-list__order-heading">
                                    <div class="my-orders-list__order-id"><span class="order-number-naming">Заказ № </span><b>2127480288</b></div>
                                    <div class="my-orders-list__order-date"><b>11.03.2018 02:15</b></div>
                                    <div class="my-orders-list__order-status"><b>Закрыт</b></div>
                                    <div class="my-orders-list__order-summ"><b>759</b></div>
                                    <div class="my-orders-list__payment-method">
                                    <? if ($i < 2) { ?>
                                        <a href="">Оплатить</a>
                                        <a class="btn btn-cancel-order-request">Отменить</a>
                                    <? } ?>
                                    </div>
                                </div>
                                <div class="my-orders-list__order-block">
                                    <div class="my-orders-list__order-block-heading">
                                        <b>№</b>
                                        <b>Товар</b>
                                        <b>Статус</b>
                                        <b>Кол-во</b>
                                        <b>Цена</b>
                                        <b>Сумма</b>
                                        <b>Способ оплаты</b>
                                    </div>
                                    <div class="my-orders-list__order-block-main">
                                        <div class="order">
                                            <div class="order__order-number column">
                                                <b>1</b>
                                            </div>
                                            <div class="order__item-info column">
                                                <div class="order__image-container">
                                                    <img src="/images/dynamic/product-image-1.jpg" alt="">
                                                </div>
                                                <div class="order__info-container">
                                                    <b>Рубашка</b>
                                                    <b>Артикул: <span>2621383</span></b>
                                                    <b>Цвет: <span>белый</span></b>
                                                    <b>Размер: <span>38</span></b>
                                                </div>
                                            </div>
                                            <div class="order__status column"><b>Закрыт</b><b>11.03.2018</b></div>
                                            <div class="order__amount column"><b>1</b></div>
                                            <div class="order__price column"><b>759</b></div>
                                            <div class="order__summ column"><b>759</b></div>
                                            <div class="order__payment-method column">
                                                <? if ($i < 2) { ?>
                                                    <a href="">Оплатить</a>
                                                    <a class="btn btn-cancel-order-request">Отменить</a>
                                                <? } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <? } ?>
                            <div class="my-orders-list__order-heading">
                                <div class="my-orders-list__order-id"><span class="order-number-naming">Заказ № </span><b>16922303</b></div>
                                <div class="my-orders-list__order-date"><b>30.01.2014 02:15</b></div>
                                <div class="my-orders-list__order-status"><b>Отказ</b></div>
                                <div class="my-orders-list__order-summ"><b>1800</b></div>
                                <div class="my-orders-list__payment-method"></div>
                            </div>
                            <div class="my-orders-list__order-block">
                                <div class="my-orders-list__order-block-heading">
                                    <b>№</b>
                                    <b>Товар</b>
                                    <b>Статус</b>
                                    <b>Кол-во</b>
                                    <b>Цена</b>
                                    <b>Сумма</b>
                                    <b>Способ оплаты</b>
                                </div>
                                <div class="my-orders-list__order-block-main">
                                    <div class="order">
                                        <div class="order__order-number column">
                                            <b>1</b>
                                        </div>
                                        <div class="order__item">
                                            <div class="order__item-info column">
                                                <div class="order__image-container">
                                                    <img src="/images/dynamic/product-image-1.jpg" alt="">
                                                </div>
                                                <div class="order__info-container">
                                                    <b>Рубашка</b>
                                                    <b>Артикул: <span>2621383</span></b>
                                                    <b>Цвет: <span>белый</span></b>
                                                    <b>Размер: <span>38</span></b>
                                                </div>
                                            </div>
                                            <div class="order__status column"><b>Отказ</b><b>30.01.2014</b></div>
                                            <div class="order__amount column"><b>1</b></div>
                                            <div class="order__price column"><b>1800</b></div>
                                            <div class="order__summ column"><b>1800</b></div>
                                            <div class="order__payment-method column"></div>
                                        </div>
                                        <div class="order__item">
                                            <div class="order__item-info column">
                                                <div class="order__image-container">
                                                    <img src="/images/dynamic/product-image-1.jpg" alt="">
                                                </div>
                                                <div class="order__info-container">
                                                    <b>Рубашка</b>
                                                    <b>Артикул: <span>2621383</span></b>
                                                    <b>Цвет: <span>белый</span></b>
                                                    <b>Размер: <span>38</span></b>
                                                </div>
                                            </div>
                                            <div class="order__status column"><b>Отказ</b><b>30.01.2014</b></div>
                                            <div class="order__amount column"><b>1</b></div>
                                            <div class="order__price column"><b>1800</b></div>
                                            <div class="order__summ column"><b>1800</b></div>
                                            <div class="order__payment-method column"></div>
                                        </div>
                                    </div>

                                    <div class="order">
                                        <div class="order__order-number column">
                                            <b>2</b>
                                        </div>
                                        <div class="order__item">
                                            <div class="order__item-info column">
                                                <div class="order__image-container">
                                                    <img src="/images/dynamic/product-image-1.jpg" alt="">
                                                </div>
                                                <div class="order__info-container">
                                                    <b>Рубашка</b>
                                                    <b>Артикул: <span>2621383</span></b>
                                                    <b>Цвет: <span>белый</span></b>
                                                    <b>Размер: <span>38</span></b>
                                                </div>
                                            </div>
                                            <div class="order__status column"><b>Отказ</b><b>30.01.2014</b></div>
                                            <div class="order__amount column"><b>1</b></div>
                                            <div class="order__price column"><b>1800</b></div>
                                            <div class="order__summ column"><b>1800</b></div>
                                            <div class="order__payment-method column"></div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <? for ($i=0; $i < 3; $i++) { ?>
                                <div class="my-orders-list__order-heading">
                                    <div class="my-orders-list__order-id"><span class="order-number-naming">Заказ № </span><b>2127480288</b></div>
                                    <div class="my-orders-list__order-date"><b>11.03.2018 02:15</b></div>
                                    <div class="my-orders-list__order-status"><b>Закрыт</b></div>
                                    <div class="my-orders-list__order-summ"><b>759</b></div>
                                    <div class="my-orders-list__payment-method"></div>
                                </div>
                                <div class="my-orders-list__order-block">
                                    <div class="my-orders-list__order-block-heading">
                                        <b>№</b>
                                        <b>Товар</b>
                                        <b>Статус</b>
                                        <b>Кол-во</b>
                                        <b>Цена</b>
                                        <b>Сумма</b>
                                        <b>Способ оплаты</b>
                                    </div>
                                    <div class="my-orders-list__order-block-main">
                                        <div class="order">
                                            <div class="order__order-number column">
                                                <b>1</b>
                                            </div>
                                            <div class="order__item-info column">
                                                <div class="order__image-container">
                                                    <img src="/images/dynamic/product-image-1.jpg" alt="">
                                                </div>
                                                <div class="order__info-container">
                                                    <b>Рубашка</b>
                                                    <b>Артикул: <span>2621383</span></b>
                                                    <b>Цвет: <span>белый</span></b>
                                                    <b>Размер: <span>38</span></b>
                                                </div>
                                            </div>
                                            <div class="order__status column"><b>Закрыт</b><b>11.03.2018</b></div>
                                            <div class="order__amount column"><b>1</b></div>
                                            <div class="order__price column"><b>759</b></div>
                                            <div class="order__summ column"><b>759</b></div>
                                            <div class="order__payment-method column"></div>
                                        </div>
                                    </div>
                                </div>
                            <? } ?>
                        </div>
                        <div class="my-orders-tab__bottom-nav">
                            <ul class="catalog-products-pagination">
                                <li><a href=""><i class="fas fa-angle-left"></i></a></li>
                                <li class="catalog-products-pagination__item"><a href="">1</a></li>
                                <li class="active"><a href="">2</a></li>
                                <li><a href="">3</a></li>
                                <li><a href=""><i class="fas fa-angle-right"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="deliveries-tab tabs-block">
                        <div class="deliveries-tab__top-nav">
                            <p>Заказов <span>31</span></p>
                            <ul class="catalog-products-pagination">
                                <li><a href=""><i class="fas fa-angle-left"></i></a></li>
                                <li class="catalog-products-pagination__item"><a href="">1</a></li>
                                <li class="active"><a href="">2</a></li>
                                <li><a href="">3</a></li>
                                <li><a href=""><i class="fas fa-angle-right"></i></a></li>
                            </ul>
                            <div class="search"><label><input type="text" placeholder="Найти"><i class="fas fa-search"></i></label></div>
                        </div>
                        <div class="deliveries-list">

                            <ul class="deliveries-list__list-heading">
                                <li>Заказ</li>
                                <li>Дата и время заказа</li>
                                <li>Адрес</li>
                                <li>Статус</li>
                                <li>Сумма</li>
                                <li>Сумма доставки</li>
                            </ul>
                            <? for ($i=0; $i < 3; $i++) { ?>
                                <div class="deliveries-list__order-heading">
                                    <div><span>Заказ № </span><b>2127480288</b></div>
                                    <div><b>28.02.2018 15:00 - 18:00</b> <a href="">Изменить</a></div>
                                    <div><b>г.Москва, ул. Коммунаров 325, кв.134</b></div>
                                    <div><b>Резерв</b></div>
                                    <div><b>5680</b></div>
                                    <div><b>400</b></div>
                                </div>
                                <div class="deliveries-list__order-block">
                                    <form id="deliveris-list-form-<?echo($i)?>" action="">
                                    <ul class="sub-list-heading">
                                        
                                        <li>Дата и время заказа</li>
                                        <li>Адрес</li>
                                        <li>Статус</li>
                                        <li>Сумма</li>
                                        <li>Сумма доставки</li>
                                    </ul>
                                    <div class="sub-order-heading">
                                        
                                        <div><b>28.02.2018 15:00 - 18:00</b> <a href="">Изменить</a></div>
                                        <div><b>г.Москва, ул. Коммунаров 325, кв.134</b></div>
                                        <div><b>Резерв</b></div>
                                        <div><b>5680</b></div>
                                        <div><b>400</b></div>
                                    </div>
                                    <div class="delete-delivery">
                                        <div class="sub-col">
                                            <b>Доставка № 16922303</b>
                                            <p>Вы можете изменить дату и время доставки</p>
                                        </div>
                                        <div class="sub-col">
                                            <a href="">Удалить из доставки</a>
                                        </div>
                                    </div>
                                    <div class="choose-delivery-time">
                                        <div class="sub-row">
                                            <b>Выберите удобный для вас способ доставки</b>
                                            <!-- Поле не доделано -->
                                            <input type="text" placeholder="Укажите интервал доставки">
                                        </div>
                                        <div class="sub-row">
                                            <input type="reset" value="Отменить">
                                            <input type="submit" value="Сохранить">
                                        </div>
                                    </div>
                                    
                                    <div class="js-wrapper js-wrapper-<?echo($i)?>">
                                        <ul class="deliveries-list-list__list-heading">
                                            <li class="select-all-inputs js-btn-autocheck"><i class="indicator"></i>Выделить все</li>
                                            <li>Товар</li>
                                            <li>Цена руб.</li>
                                            <li>Номер заказа</li>
                                        </ul>
                                        <ul class="deliveries-list-list__item-list">
                                            <? for ($j=0; $j < 2; $j++) { ?>
                                            <li>
                                                <input class="js-input-autocheck" type="checkbox" name="move-to-archive" value="24977875" id="appeal-id-<?echo($i)?>-<?echo($j)?>">
                                                <label class="js-label-autocheck" for="appeal-id-<?echo($i)?>-<?echo($j)?>">
                                                    <div class="item">
                                                        <div class="item__checkbox-container column">
                                                            <i></i>
                                                        </div>
                                                        <div class="item__item-info column">
                                                            <div class="item__image-container">
                                                                <img src="/images/dynamic/product-image-1.jpg" alt="">
                                                                <i class="item__flag-sale active">SALE</i>
                                                            </div>
                                                            <div class="item__info-container">
                                                                <b>Рубашка</b>
                                                                <b>Артикул: <span>2621383</span></b>
                                                                <b>Цвет: <span>белый</span></b>
                                                                <b>Размер: <span>38</span></b>
                                                            </div>
                                                        </div>
                                                        <div class="column">249</div>
                                                        <div class="column">2127480288</div>
                                                    </div>
                                                </label>
                                            </li>
                                            <? } ?>
                                        </ul>
                                    </div>

                                </form>
                                </div>
                            <? } ?>

                        </div>
                        <div class="deliveries-tab__bottom-nav">
                            <ul class="catalog-products-pagination">
                                <li><a href=""><i class="fas fa-angle-left"></i></a></li>
                                <li class="catalog-products-pagination__item"><a href="">1</a></li>
                                <li class="active"><a href="">2</a></li>
                                <li><a href="">3</a></li>
                                <li><a href=""><i class="fas fa-angle-right"></i></a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="rate-service-tab tabs-block">
                        <div class="rate-service-tab__top-nav">
                            <p>Заказов <span>31</span></p>
                            <ul class="catalog-products-pagination">
                                <li><a href=""><i class="fas fa-angle-left"></i></a></li>
                                <li class="catalog-products-pagination__item"><a href="">1</a></li>
                                <li class="active"><a href="">2</a></li>
                                <li><a href="">3</a></li>
                                <li><a href=""><i class="fas fa-angle-right"></i></a></li>
                            </ul>
                            <div class="search"><label><input type="text" placeholder="Найти"><i class="fas fa-search"></i></label></div>
                        </div>

                        <div class="rate-service-list">
                            <ul class="rate-service-list__list-heading">
                                <li>Доставка</li>
                                <li>Дата и время доставки</li>
                                <li>Адрес</li>
                                <li>Имя Курьера</li>
                                <li>Оценить обслуживание</li>
                            </ul>
                            <div class="rate-service-list__order-heading">
                                <div><span>Заказ № </span><b>2127480288</b></div>
                                <div><b>11.03.2018 02:15</b></div>
                                <div><b>Самовывоз. Москва, ул.Горького 34/3 кв. 20</b></div>
                                <div><b></b></div>
                                <div><button class="btn show-modal" id="btn-show-modal-rate-delivery-service"><b>Оценить</b> <b>обслуживание</b></button></div>
                            </div>
                            <div class="rate-service-list__order-heading">
                                <div><span>Заказ № </span><b>2127480288</b></div>
                                <div><b>11.03.2018 02:15</b></div>
                                <div><b>Самовывоз. Москва, ул.Горького 34/3 кв. 20</b></div>
                                <div><b>Николай</b></div>
                                <div><button class="btn show-modal" id="btn-show-modal-rate-delivery-service"><b>Оценить</b> <b>обслуживание</b></button></div>
                            </div>
                            <div class="rate-service-list__order-heading">
                                <div><span>Заказ № </span><b>2127480288</b></div>
                                <div><b>11.03.2018 02:15</b></div>
                                <div><b>Самовывоз. Москва, ул.Горького 34/3 кв. 20</b></div>
                                <div><b>Кирилл</b></div>
                                <div><button class="btn show-modal" id="btn-show-modal-rate-delivery-service"><b>Оценить</b> <b>обслуживание</b></button></div>
                            </div>
                        </div>

                        <div class="rate-service-tab__bottom-nav">
                            <ul class="catalog-products-pagination">
                                <li><a href=""><i class="fas fa-angle-left"></i></a></li>
                                <li class="catalog-products-pagination__item"><a href="">1</a></li>
                                <li class="active"><a href="">2</a></li>
                                <li><a href="">3</a></li>
                                <li><a href=""><i class="fas fa-angle-right"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>