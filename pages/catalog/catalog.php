<div class="page page-catalog">
    <div class="page-line top">
        <div class="page-line__inner top__inner">
            <div class="top-title">
                <span>Рубашки</span>
            </div>
            <div class="top-breadcrumbs">
                <a href="">Главная</a>
                <a href="">Рубашки</a>
                <span>Casual</span>
            </div>
        </div>
    </div>
    <div class="page-line catalog">
        <div class="page-line__inner catalog__inner">
            <div class="col-1">
                <div class="catalog-sections">
                    <div class="catalog-sections__heading">
                        <span>Каталог</span>
                    </div>
                    <ul class="catalog-sections__list">
                        <li><a href="">Рубашка <span>(10)</span></a></li>
                        <li><a href="">Бабочки <span>(10)</span></a></li>
                        <li><a href="">Галстуки <span>(10)</span></a></li>
                        <li><a href="">Подтяжки <span>(10)</span></a></li>
                        <li><a href="">Носки <span>(10)</span></a></li>      
                        <li><a href="">Запонки <span>(10)</span></a></li>       
                        <li><a href="">Сумки <span>(10)</span></a></li>       
                        <li><a href="">Портмоне <span>(10)</span></a></li>
                        <li><a href="">Шейные платки <span>(10)</span></a></li>
                        <li><a href="">Платки паше <span>(10)</span></a></li>
                        <li><a href="">Зажимы <span>(10)</span></a></li>
                        <li><a href="">Булавки <span>(10)</span></a></li>
                        <li><a href="">Броши <span>(10)</span></a></li>   
                        <li><a href="">Шарфы <span>(10)</span></a></li>
                    </ul>
                </div>
                <form action="" nethod="POST" class="catalog-filter-form">
                    <div class="catalog-filter">
                        <div class="catalog-filter__block"> <!-- Стиль -->
                            <div class="catalog-filter__heading">
                                <span>Стиль</span>
                            </div>
                            <div class="catalog-filter__content">
                                <label class="catalog-filter-checkbox">
                                    <input type="checkbox">
                                    <span>classic рубашки</span>
                                </label>
                                <label class="catalog-filter-checkbox">
                                    <input type="checkbox">
                                    <span>casual рубашки</span>
                                </label>
                                <label class="catalog-filter-checkbox">
                                    <input type="checkbox">
                                    <span>под бабочку</span>
                                </label>
                                <label class="catalog-filter-checkbox">
                                    <input type="checkbox">
                                    <span>под запонки</span>
                                </label>
                            </div>
                        </div>

                        <div class="catalog-filter__block"> <!-- Цена -->
                            <div class="catalog-filter__heading">
                                <span>Цена <span>(руб.)</span></span>
                            </div>
                            <div class="catalog-filter__content">
                                <div class="catalog-filter__price-slider"></div>
                                <input class="catalog-filter__price-val catalog-filter__price-val-min" type="text" value="0">
                                <input class="catalog-filter__price-val catalog-filter__price-val-max" type="text" value="2500">
                            </div>
                        </div>

                        <div class="catalog-filter__block"> <!-- Цвет -->
                            <div class="catalog-filter__heading">
                                <span>Цвет</span>
                            </div>
                            <div class="catalog-filter__content">
                                <label class="catalog-filter__color-checkbox">
                                    <input type="checkbox" name="" id="">
                                    <img style="background: #111111;" src="" alt="">
                                    <div class="popup">Оранжевый</div>
                                </label>
                                <label class="catalog-filter__color-checkbox">
                                    <input type="checkbox" name="" id="">
                                    <img style="background: #ffffff;" src="" alt="">
                                    <div class="popup">Оранжевый</div>
                                </label>
                                <label class="catalog-filter__color-checkbox">
                                    <input type="checkbox" name="" id="">
                                    <img style="background: #dea65f;" src="" alt="">
                                    <div class="popup">Оранжевый</div>
                                </label>
                                <label class="catalog-filter__color-checkbox">
                                    <input type="checkbox" name="" id="">
                                    <img style="background: #f26c4f;" src="" alt="">
                                    <div class="popup">Оранжевый</div>
                                </label>
                                <label class="catalog-filter__color-checkbox">
                                    <input type="checkbox" name="" id="">
                                    <img style="background: #f5989d;" src="" alt="">
                                    <div class="popup">Оранжевый</div>
                                </label>
                                <label class="catalog-filter__color-checkbox">
                                    <input type="checkbox" name="" id="">
                                    <img style="background: #c2c2c2;" src="" alt="">
                                    <div class="popup">Оранжевый</div>
                                </label>
                                <label class="catalog-filter__color-checkbox">
                                    <input type="checkbox" name="" id="">
                                    <img style="background: #869ee3;" src="" alt="">
                                    <div class="popup">Оранжевый</div>
                                </label>
                                <label class="catalog-filter__color-checkbox">
                                    <input type="checkbox" name="" id="">
                                    <img style="background: #464646;" src="" alt="">
                                    <div class="popup">Оранжевый</div>
                                </label>
                                <label class="catalog-filter__color-checkbox">
                                    <input type="checkbox" name="" id="">
                                    <img style="background: #febca9;" src="" alt="">
                                    <div class="popup">Оранжевый</div>
                                </label>
                                <label class="catalog-filter__color-checkbox">
                                    <input type="checkbox" name="" id="">
                                    <img style="background: #ebebeb;" src="" alt="">
                                    <div class="popup">Оранжевый</div>
                                </label>
                                <label class="catalog-filter__color-checkbox">
                                    <input type="checkbox" name="" id="">
                                    <img style="background: #f26d7d;" src="" alt="">
                                    <div class="popup">Оранжевый</div>
                                </label>
                                <label class="catalog-filter__color-checkbox">
                                    <input type="checkbox" name="" id="">
                                    <img style="background: #fff799;" src="" alt="">
                                    <div class="popup">Оранжевый</div>
                                </label>
                            </div>
                        </div>

                        <div class="catalog-filter__block"> <!-- Размер -->
                            <div class="catalog-filter__heading">
                                <span>Размер</span>
                            </div>
                            <div class="catalog-filter__content">
                                <label class="catalog-filter__size-checkbox"><input type="checkbox" name="" id=""><span>37</span></label>
                                <label class="catalog-filter__size-checkbox"><input type="checkbox" name="" id=""><span>38</span></label>
                                <label class="catalog-filter__size-checkbox"><input type="checkbox" name="" id=""><span>39</span></label>
                                <label class="catalog-filter__size-checkbox"><input type="checkbox" name="" id=""><span>40</span></label>
                                <label class="catalog-filter__size-checkbox"><input type="checkbox" name="" id=""><span>41</span></label>
                                <label class="catalog-filter__size-checkbox"><input type="checkbox" name="" id=""><span>42</span></label>
                                <label class="catalog-filter__size-checkbox"><input type="checkbox" name="" id=""><span>43</span></label>
                                <label class="catalog-filter__size-checkbox"><input type="checkbox" name="" id=""><span>44</span></label>
                                <label class="catalog-filter__size-checkbox"><input type="checkbox" name="" id=""><span>45</span></label>
                                <label class="catalog-filter__size-checkbox"><input type="checkbox" name="" id=""><span>46</span></label>
                                <label class="catalog-filter__size-checkbox"><input type="checkbox" name="" id=""><span>47</span></label>
                                <label class="catalog-filter__size-checkbox"><input type="checkbox" name="" id=""><span>48</span></label>
                            </div>
                        </div>

                        <div class="catalog-filter__block"> <!-- Состав -->
                            <div class="catalog-filter__heading">
                                <span>Состав</span>
                            </div>
                            <div class="catalog-filter__content">
                                <label class="catalog-filter-checkbox">
                                    <input type="checkbox">
                                    <span>хлопок</span>
                                </label>
                                <label class="catalog-filter-checkbox">
                                    <input type="checkbox">
                                    <span>хлопок/лен</span>
                                </label>
                                <label class="catalog-filter-checkbox">
                                    <input type="checkbox">
                                    <span>хлопок/эластан</span>
                                </label>
                                <label class="catalog-filter-checkbox">
                                    <input type="checkbox">
                                    <span>хлопок/вискоза</span>
                                </label>
                                <label class="catalog-filter-checkbox">
                                    <input type="checkbox">
                                    <span>хлопок/шелк</span>
                                </label>
                                <label class="catalog-filter-checkbox">
                                    <input type="checkbox">
                                    <span>лен/шелк</span>
                                </label>
                            </div>
                        </div>

                        <div class="catalog-filter__block"> <!-- Обработка -->
                            <div class="catalog-filter__heading">
                                <span>Обработка</span>
                            </div>
                            <div class="catalog-filter__content">
                                <label class="catalog-filter-checkbox">
                                    <input type="checkbox">
                                    <span>Easy care</span>
                                    <div class="popup">Наши рубашки Easy Care требуют меньше ухода и глажения, чем обычные.</div>
                                </label>
                                <label class="catalog-filter-checkbox">
                                    <input type="checkbox">
                                    <span>Oeko-Tex</span>
                                    <div class="popup">Независимый сертификат, который гарантирует во-первых, что состовляющее нашей продукции не вредят здоровью, во-вторых, процесс производства и разработки является экологически безопасным для окружающий среды</div>
                                </label>
                            </div>
                        </div>
                        
                        <div class="catalog-filter__block"> <!-- Силуэт -->
                            <div class="catalog-filter__heading">
                                <span>Силуэт</span>
                            </div>
                            <div class="catalog-filter__content">
                                <label class="catalog-filter-checkbox">
                                    <input type="checkbox">
                                    <span>приталенные (slim fit)</span>
                                </label>
                                <label class="catalog-filter-checkbox">
                                    <input type="checkbox">
                                    <span>полуприталенные (regular fit)</span>
                                </label>
                                <label class="catalog-filter-checkbox">
                                    <input type="checkbox">
                                    <span>прямые (classic fit)</span>
                                </label>
                            </div>
                        </div>

                        <div class="catalog-filter__block"> <!-- Рост -->
                            <div class="catalog-filter__heading">
                                <span>Рост</span>
                            </div>
                            <div class="catalog-filter__content">
                                <div class="catalog-filter__height-slider"></div>
                                <input class="catalog-filter__height-val catalog-filter__height-val-min" type="text" value="0">
                                <input class="catalog-filter__height-val catalog-filter__height-val-max" type="text" value="200">
                            </div>
                        </div>

                        <div class="catalog-filter__block"> <!-- Рукав -->
                            <div class="catalog-filter__heading">
                                <span>Рукав</span>
                            </div>
                            <div class="catalog-filter__content">
                                <label class="catalog-filter-checkbox">
                                    <input type="checkbox">
                                    <span>длинный</span>
                                </label>
                                <label class="catalog-filter-checkbox">
                                    <input type="checkbox">
                                    <span>короткий</span>
                                </label>
                            </div>
                        </div>
                        
                        <div class="catalog-filter__block"> <!-- Дизайн -->
                            <div class="catalog-filter__heading">
                                <span>Дизайн</span>
                            </div>
                            <div class="catalog-filter__content">
                                <label class="catalog-filter-checkbox">
                                    <input type="checkbox">
                                    <span>однотонные</span>
                                </label>
                                <label class="catalog-filter-checkbox">
                                    <input type="checkbox">
                                    <span>в полоску</span>
                                </label>
                                <label class="catalog-filter-checkbox">
                                    <input type="checkbox">
                                    <span>в клетку</span>
                                </label>
                                <label class="catalog-filter-checkbox">
                                    <input type="checkbox">
                                    <span>с рисунком</span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <input class="catalog-filter-form__submit" type="submit" value="Показать 56 товаров">
                </form>
            </div>
            <div class="col-2">
                <div class="catalog-products">
                    <div class="catalog-products-notify">
                        <span>Дополнительно <span class="catalog-products-notify_special">-25%</span> почти на всё. Условия акции</span>
                    </div>
                    <div class="catalog-products-top">
                        <div class="catalog-products-count">Товаров найдено <span>103</span></div>
                        <div class="catalog-products-sort">
                            <div class="catalog-products-sort__heading">Сортировка:<span>по обновлению</span></div>
                            <div class="catalog-products-sort__block">
                                <ul>
                                    <li><a href="">Цена по убыванию</a></li>
                                    <li><a href="">Цена по возрастанию</a></li>
                                    <li><a href="">По скидке</a></li>
                                    <li><a href="">По обновлению</a></li>
                                    <li><a href="">По популярности</a></li>
                                    <li><a href="">По рейтингу</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="catalog-products-display-count">
                            <div class="catalog-products-display-count__heading">Показывать по:</div>
                            <div class="catalog-products-display-count__block">
                                <a href="">40</a>
                                <a href="" class="active">100</a>
                                <a href="">200</a>
                            </div>
                        </div>
                        <div class="catalog-products-display-type"></div>
                    </div>

                    <div class="catalog-products-list">
                        <?for ($i=0; $i < 20; $i++) { 
                            include('blocks/products-list-item-full.php');
                        }?>
                    </div>

                    <div class="catalog-products-bottom">
                        <ul class="catalog-products-pagination">
                            <li><a href=""><i class="fas fa-angle-left"></i></a></li>
                            <li class="catalog-products-pagination__item"><a href="">1</a></li>
                            <li class="active"><a href="">2</a></li>
                            <li><a href="">3</a></li>
                            <li><a href="">4</a></li>
                            <li><a href="">5</a></li>
                            <li><a href="">6</a></li>
                            <li><a href="">7</a></li>
                            <li><a href="">8</a></li>
                            <li><a href="">9</a></li>
                            <li><a href="">10</a></li>
                            <li><a href=""><i class="fas fa-angle-right"></i></a></li>
                        </ul>
                        <div class="catalog-products-display-count">
                            <div class="catalog-products-display-count__heading">Показывать по:</div>
                            <div class="catalog-products-display-count__block">
                                <a href="">40</a>
                                <a href="" class="active">100</a>
                                <a href="">200</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="page-line bottom">
        <div class="page-line__inner bottom__inner">
            <div class="catalog-products-slider">
                <div class="catalog-products-slider__heading">
                    <span>Недавно смотрели</span>
                </div>
                <div class="catalog-products-slider__block products-slider">
                    <?for ($i=0; $i < 20; $i++) { 
                        include('blocks/products-list-item.php');
                    }?>
                </div>
            </div>
        </div>
    </div>
</div>