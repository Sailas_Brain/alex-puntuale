<div class="page page-my-balance">
    <div class="page-line my-balance">
        <div class="page-line__inner my-balance__inner">
            <div class="col-1">
                <?include('blocks/sidebar-menu.php');?>
            </div>
            <div class="col-2">
                <h1 class="my-balance__title">Мой баланс</h1>
                    <div class="my-balance__operations-history operations-internal-block">
                        <div class="operations-internal-block__text">
                            <p>Здесь Вы можете хранить денежные средства для дальнейшей оплаты товаров и услуг в нашем интернет-магазине.</p>
                            <p>В истории операций Вы можете увидеть все финансовые операции, проведенные в интернет-магазине.</p>
                            <button class="button-ask">история операций</button>
                        </div>
                        <div class="operations-internal-block__click-operarion-block internal-block">

                            <span class="internal-block__text">Для того, чтобы посмотреть историю операций, выберите дату и нажмите на 
                            кнопку «Запросить». Формирование данной таблицы может занять некоторое время.</span>
                            <div class="internal-block__top">
                                <img src="/images/my-balance/vector.png" alt="">
                                <span>История операций с</span>
                                    <label class="datepicker__label">
                                        <input class="datepicker" type="text" id="datepicker-from" name="birthday" placeholder="14.02.2018">
										<i class="datepicker__icon"></i>
									</label>
                                        <span>по</span>
                                    <label class="datepicker__label">
                                        <input class="datepicker" type="text" id="datepicker-to" name="birthday" placeholder="14.03.2018">
										<i class="datepicker__icon"></i>
                                    </label>
                                <button>ЗАПРОСИТЬ</button>                                 
                            </div>
                            <div class="internal-block__bottom">
                                <p>История операций от 21.03.2018 с 21.12.2017 по 21.03.2018:</p>
                                <div class="text-info">
                                    <span>Дата</span>
                                    <span>Приход, руб.</span>
                                    <span>Расход, руб.</span>
                                    <span>Способ оплаты</span>
                                    <span>Операция</span>
                                </div>
                                <div class="info-operation">
                                    <? for($i = 0; $i < 4; $i++){ ?>
                                    <div class="info-operation__info">
                                    <span>21.12.2017</span>
                                    <span>726</span>
                                    <span>726</span>
                                    <span>Курьеру</span>
                                    <span>Продажа</span>
                                    </div>
                                    <? } ?>
                                </div>
                                <div class="export">
                                    <a href="#">Экспорт в CSV</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="my-balance__monetary-facilities internal-block-facilities">
                        <div class="internal-block-facilities__cash block-your-balance">
                            <div class="block-your-balance__balance">
                                <img src="/images/my-balance/purse.png" alt="">
                                <p>0 РУБ.</p>
                                <p>БАЛАНС</p>
                                <p>Денежные средства, находящиеся на Вашем балансе, включая заблокированные суммы</p>
                            </div>
                            <div class="block-your-balance__locked">
                            <img src="/images/my-balance/locked.png" alt="">
                                <p>0 РУБ.</p>
                                <p>ЗАБЛОКИРОВАНО</p>
                                <p>Временно зарезервированные суммы по совершенным 
                                операциям до момента подтверждения или отказа от операции</p>
                            </div>
                            <div class="block-your-balance__available-balance">
                                <img src="/images/my-balance/mone.png" alt="">
                                <p>0 РУБ.</p>
                                <p>ДОСТУПНЫЙ ОСТАТОК</p>
                                <p>Доступная сумма денежных средств Вашего баланса за исключением зарезервированной суммы</p>
                            </div>
                        </div>
                        <div class="internal-block-facilities__top-up-balance internal-blocks">
                            <div class="internal-blocks__common-blocks">
                                <img src="/images/my-balance/card-mone.png" alt="">
                                <span>Пополнить баланс</span>
                            </div>
                            <div class="internal-blocks__common-blocks">
                                <img src="/images/my-balance/pergament.png" alt="">
                                <span class="button-detail">Детализация подарочных средств</span>
                            </div>
                            <div class="internal-blocks__common-blocks">
                                <img src="/images/my-balance/g-mail.png" alt="">
                                <span>Запросить детализацию</span>
                            </div>
                            <div class="internal-blocks__common-blocks">
                                <img src="/images/my-balance/hand.png" alt="">
                                <span class="another">Вернуть деньги</span>
                            </div>
                        </div>
                    </div>
                    <div class="my-balance__the-bility-to-pay internal-block">
                        <div class="internal-block__title">
                            <p>ПОЛУЧИТЕ ВОЗМОЖНОСТЬ ОПЛАЧИВАТЬ ЗАКАЗЫ В РАССРОЧКУ</p>
                            <span>Условия рассрочки</span>
                        </div>
                        <div class="internal-block__text">
                            <span>Для получения индивидуального предложения заполните анкету.
                            В случае получения одобрения, Вам будет предоставлен лимит средств,
                             которые могут быть использованы для осуществления покупок.<br /></span>
                            <span>Договор по рассрочке заключается только на человека,
                            которому принадлежит личный кабинет.</span>
                        </div>
                        <div class="internal-block__button">
                            <button type="submit" class="btn btn-submit">ЗАПОЛНИТЬ АНКЕТУ</button>
                        </div>
                    </div>
                    <div class="my-balance__gift-certificate internal-blocks">
                        <div class="internal-blocks__images">
                        <img src="/images/my-balance/bantik.png" alt="">
                        </div>
                        <div class="internal-blocks__text">
                            <p>Активация подарочного электронного сертификата</p>
                            <span>После активации сертификата, подарочные средства будут зачислены на ваш бонусный счет.
                            <br>Вы будете дополнительно оповещены об активации по СМС.</span> 
                        </div>
                        <div class="internal-blocks__activation">
                                <input type="text" name="" placeholder="Номер сертификата">
                                <button>АКТИВИРОВАТЬ</button> 
                                <div class="text">
                                <span class="button-certificate">Ваши сертификаты</span>
                                </div>
                        </div>  
                    </div>
                    <div class="my-balance__top-up-balance internal-block">
                        <span class="my-balance__title">Пополнить баланс</span>
                        <div class="internal-block__top">
                            <span>В истории операций Вы можете увидеть все финансовые операции, проведенные в интернет-магазине.<br></span>
                            <span>Сумма</span>
                            <input type="text" name="" placeholder="1500">
                            <span>*Максимальная сумма пополнения устанавливается платежной системой</span>
                        </div>
                        <div class="internal-block__bottom">
                                <span>Выберите способ пополнения:</span>
                                <div class="checkbox">
                                    <label for="way">
                                    <input id="way" type="checkbox" name="checkbox">
                                    <span>Онлайн оплата картой</span>
                                </label>
                                </div>
                        </div>
                    </div>
                    <div class="my-balance__refund-means internal-block">
                        <span class="my-balance__title">Возврат денежных средств</span>
                        <div class="internal-block__text">
                            <span>Вернуть денежные средства с Вашего баланса просто. Для этого Вам нужно оформить заявку.<br>
                            Введите желаемую сумму и укажите банковские реквизиты, на которые необходимо перевести денежные средства.<br>
                            Срок перечисления денежных средств осуществляется в срок до 10 календарных дней. Срок зачисления денежных
                            средств<br> на расчетный счет Клиента зависит от внутреннего регламента банка-получателя.</span>
                        </div>
                        <div class="internal-block__amount">
                            <span>Сумма (руб)</span>
                            <input type="text" name="" placeholder="1500">
                            <span>*Сумма возврата не может превышать доступный остаток. </span>
                        </div>
                        <div class="internal-block__chosen-method">
                            <span>Выберите способ пополнения:</span>
                                <div class="checkbox">
                                    <label for="way-innet">
                                    <input id="way-innet" type="checkbox" name="checkbox">
                                    <span>Онлайн оплата картой</span>
                                </label>
                                </div>
                        </div>
                        <div class="internal-block__reqisite">
                            <span>Выберите или добавьте реквизиты на которые необходимо сделать возврат денежных средств.</span>
                            <div class="icon">
                            <img src="/images/my-balance/exclamation.png" alt="">
                            </div>
                            <div class="text">
                                <p>Уважаемые Клиенты! Обращаем Ваше внимание на то, что в связи с ограничениями стандартов 
                                безопасности<br> платежных шлюзов PCI DSS, возникают трудности с возвратами денежных средств на банковские счета,
                                начинающиеся<br> на 302*******. Ограничения связаны с передачей номера карты для совершения транзакции.</p>
                                <p>В случае введения некорректных реквизитов, денежные средства не будут перечислены в указанные сроки.</p>
                            </div>
                        </div>
                        <div class="internal-block__buttons">
                            <button>ДОБАВИТЬ РЕКВИЗИТЫ</button>
                            <button>ОФОРМИТЬ ВОЗВРАТ</button>
                            <div class="text">
                                <img src="/images/my-balance/arrow.png" alt="">
                                <span>Реквизиты не добавлены</span>
                            </div>
                    </div>
                    <div class="my-balance__in-this-section-you-can in-this-section-you-can-block">
                        <span class="my-balance__title">В этом разделе вы можете:</span>
                        <div class="col-1-1">
                            <div class="in-this-section-you-can-block__left-block internal-block">
                                <div class="internal-block__payment-for-goods">
                                    <div class="images">
                                        <img src="/images/my-balance/color-mone.png" alt="">
                                    </div>
                                    <div class="text">
                                        <span>Оплата товаров и услуг в нашем интернет-магазине
                                        в полном объёме или частично доступными средствами
                                        Вашего баланса.</span>
                                        <p>Вернуть денежные средства с Вашего баланса просто. Для этого Вам нужно оформить <br>заявку.<br>
                                        Введите желаемую сумму и укажите банковские реквизиты, на которые необходимо перевести денежные средства.
                                        Срок перечисления денежных средств осуществляется в срок до 10 календарных дней.
                                        Срок зачисления денежных средств на расчетный счет Клиента зависит от внутреннего регламента банка-получателя.</p>
                                    </div>
                                </div>
                                <div class="internal-block__tanck-your-transactions">
                                <div class="internal-block__payment-for-goods">
                                    <div class="images">
                                        <img src="/images/my-balance/optik.png" alt="">
                                    </div>
                                    <div class="text">
                                        <span>Отслеживать Ваши операции.</span>
                                        <p><span>История операций </span>– это 
                                        перечень совершённых вами операций за интересующий 
                                        Вас период не ранее чем за последние 183 дня</p>
                                        <p><span>Детализация </span>– это перечень совершенных 
                                        Вами операций (например, оплаченный заказ средствами 
                                        Вашего баланса, но еще не полученный Покупателем), 
                                        суммы по которым временно зарезервированы.</p>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-1-2">
                            <div class="in-this-section-you-can-block__right-block internal-block">
                            <div class="internal-block__return-partial">
                                    <div class="images">
                                        <img src="/images/my-balance/color-hend.png" alt="">
                                    </div>
                                    <div class="text">
                                        <span>Вернуть частично или в полном объёме доступные средства Вашего баланса на указанные Вами реквизиты</span>
                                        <p>Вернуть денежные средства с Вашего баланса просто. Для этого
                                        Вам нужно всего лишь оформить заявку на возврат денежных средств (ДС).
                                        Для этого нажмите на ссылку «Вернуть ДС» под отображением Ваших доступных
                                        денежных средств в окне «Доступный остаток», указав Ваши банковские реквизиты, на которые
                                        Вы хотите перевести желаемую сумму доступных денежных средств Вашего баланса.</p>
                                    </div>
                                </div>
                                <div class="internal-block__pay-attention">
                                    <div class="icon">
                                        <img src="/images/my-balance/exclamation.png" alt="">
                                    </div>
                                    <div class="text">
                                        <span>Обратите внимание!</span>
                                        <p>В случае возврата товара надлежащего качества после оплаты, 
                                        баланс будет увеличен на сумму возвращенного товара. В случае возврата товара 
                                        после оплаты по причине брака, баланс будет увеличен на сумму
                                        возвращенного товара после подтверждения несоответствия.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>