<div class="page page-discount">
    <div class="page__inner page-discount__inner">
        <div class="col-1">
            <?include('blocks/sidebar-menu.php');?>
        </div>
        <div class="col-2">
            <div class="info">
                <div class="info__my-discount">
                    <b class="info__discount-title">Моя скидка</b>
                    <b class="info__discount-percentage">0<span class="info__superscript">%</span></b>
                </div>
                <p>Скидка рассчитывается на основе покупок, сделанных через Личный кабинет, и зависит от суммы выкупа и процента выкупа заказанных товаров за последние полгода (183 дня). Если Клиент не совершает покупок и возвратов в течение полугода, процент выкупа рассчитывается за весь период сотрудничества.</p>
                <button class="btn btn-show-modal-calculate-discount" href="">Как рассчитать?</button>
            </div>
            <div class="limitations-info">
                <div class="accordion">
                    <div class="accordion__heading">
                        <h3>Ограничения действия скидки постоянного покупателя</h3>
                    </div>
                    <div class="accordion__block">
                        <p>Максимальная скидка постоянного покупателя на разделы: игрушки, книги и диски, красота, товары для дома, бренды Kapika, Reima, Lassie by Reima  равна 7%.</p>
                        <p>Скидка покупателя не распространяется на электронику, презервативы, спортпит и БАДы, товары для малышей, транспорт, спорттовары, на товары, стоимость которых составляет 700 рублей и менее (с учётом всех скидок и промокодов).</p>
                    </div>
                </div>
                <div>
                    <div class="limitations-info__card">
                        <b class="limitations-info__title">Сумма выкупа</b>
                        <div class="hover-popup">
                            <p>Сумма, на которую Вы совершили покупки в нашем магазине с момента первого заказа.</p>
                        </div>
                        <b class="limitations-info__value">29 553 руб.</b>
                    </div>
                    <div class="limitations-info__card">
                        <b class="limitations-info__title">Процент выкупа</b>
                        <div class="hover-popup">
                            <p>Отношение стоимости выкупленного товара к сумме выкупленного и возвращенного товара в процентах, рассчитанное за последние полгода.</p>
                        </div>
                        <b class="limitations-info__value">22,61%</b>
                        <button class="btn btn-show-modal-calculate-buyout">Как рассчитать?</button>
                    </div>
                    <div class="limitations-info__card">
                        <b class="limitations-info__title">Доставка без предоплаты</b>
                        <div class="hover-popup hover-popup--bigger">
                            <p>Количество и сумма товаров, которое можно оформить на доставку без предварительной оплаты (если иное не оговорено в п. 6.2.4 Публичной оферты). Данное количество и сумма товаров зависит от Вашего процента выкупа и суммы выкупа.</p>
                        </div>
                        <b class="limitations-info__value">До 30 шт.</b>
                        <button class="btn btn-show-modal-calculate-delivery">Как рассчитать?</button>
                    </div>
                </div>  
            </div>
        </div>
    </div>
</div>