<div class="page page-my-data">
    <div class="page-line my-data">
        <div class="page-line__inner my-data__inner">
            <div class="col-1">
                <?include('blocks/sidebar-menu.php');?>
            </div>
            <div class="col-2">        
                <h1 class="my-data__title">Мои данные</h1>
                    <div class="col-2-1">
                        <div class="my-data__personals">
                            <span class="my-data__title-for-block">Персональные данные</span>
                            <div class="my-data__info-personal personal-block">
                                <div class="personal-block__left">
                                    <p>ФИО:</p>
                                    <p>E-mail:</p>
                                    <p>Дата рождения:</p>
                                    <p>Пол:</p>
                                </div>
                                <div class="personal-block__right">
                                    <p>Щербинина Кристина Сергеевна</p>
                                    <p>kristinaa89@bk.ru</p>
                                    <p>23 мая 1989 г.</p>
                                    <p>Женский</p>
                                </div>
                            </div>
                                <div class="my-data__change">
                                    <span class="button-change">Изменить персональные данные</span>
                                </div>
                                <div class="my-data__button-confirm">
                                    <a href="#">ПОДТВЕРДИТЬ</a>
                                </div>
                        </div>
                        <div class="my-data__shape-option">
                        <span class="my-data__title-for-block">Параметры фигуры</span>
                            <div class="my-data__option block-shape-option">
                                <div class="block-shape-option__left">
                                    <p>Рост (см)</p>
                                    <p>Вес (кг)</p>
                                    <p>Воротник (см)</p>
                                    <p>Плечи (см)</p>
                                    <p>Живот (см)</p>
                                    <p>Бицепс (см)</p>
                                    <p>Длина рукова (см)</p>
                                    <p>Грудь (см)</p>
                                    <p>Бедра (см)</p>
                                    <p>Талия (см)</p>
                                    <p>Обхват запястья (см)</p>
                                    <p>Длина рубашки (см)</p>
                                </div>
                                <div class="block-shape-option__right">
                                    <div class="column-1">
                                        <p>-</p>
                                        <p>-</p>
                                        <p>-</p>
                                        <p>-</p>
                                        <p>-</p>
                                        <p>-</p>
                                        <p>-</p>
                                        <p>-</p>
                                        <p>-</p>
                                        <p>-</p>
                                        <p>-</p>
                                        <p>-</p>
                                    </div>
                                    <div class="column-2">
                                        <p>-</p>
                                        <p>-</p>
                                        <p>-</p>
                                        <p>-</p>
                                        <p>-</p>
                                        <p>-</p>
                                        <p>-</p>
                                        <p>-</p>
                                        <p>-</p>
                                        <p>-</p>
                                        <p>-</p>
                                        <p>-</p>
                                    </div>
                                </div>
                            </div>
                            <div class="my-data__buttom-modal-window">
                                <span class="button-parametrs">Изменить параметры</span>
                            </div>           
                        </div>
                        <div class="my-data__dispatch">
                            <span class="my-data__title-for-block">Рассылки</span>
                            <div class="my-data__dispatch-internal  dispatch-block">
                                <div class="dispatch-block__top">
                                    <p>Вы можете подписаться на следующие виды 
                                        рассылок нашего магазина для получения новостей 
                                        про акции и <a href="#">промокоды:</a></p>
                                </div>
                                <div class="dispatch-block__center">
                                    <span class="dispatch-block__email">Email рассылки</span>
                                    <div class="checkbox">
                                        <label class="" for="dispatch">
                                            <input id="dispatch" name="checkbox" type="checkbox">
                                            <span>Акции, персональные промокоды и секретные распродажи</span>
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        
                                        <label class="" for="dispatch-1">
                                            <input id="dispatch-1" name="checkbox" type="checkbox">
                                            <span>Лист ожидания</span>
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        
                                        <label class="" for="dispatch-2">
                                            <input id="dispatch-2" name="checkbox" type="checkbox">
                                            <span>Личные рекомендации товаров</span>
                                        </label>
                                    </div>
                                </div>
                                <div class="dispatch-block__bottom">
                                <span class="dispatch-block__email">SMS рассылки</span>
                                    <div class="checkbox">
                                        <label class="" for="dispatch-3">
                                            <input id="dispatch-3" name="checkbox" type="checkbox">
                                            <span>SMS-сообщения</span>
                                        </label>
                                    </div>
                                    <div class="my-data__button-confirm">
                                        <a href="#">СОХРАНИТЬ</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="my-data__deliting-cabinet">
                            <span class="my-data__title-for-block">Удаление личного кабинета</span>
                            <div class="my-data__deliting-cabinet-internal deliting-cabinet-block">
                                <div class="deliting-cabinet-block__text">
                                    <p>Как только Ваш Личный Кабинет будет удален, Вы
                                        автоматически выйдете из системы и больше не
                                        сможете войти в этот аккаунт.</p>
                                </div>
                            </div>
                            <div class="my-data__buttom-modal-window">
                                <span>Удалить личный кабинет</span>
                            </div> 
                        </div>
                        <div class="my-data__social-networks">
                            <span class="my-data__title-for-block">Социальные сети</span>
                            <div class="my-data__social-networks-internal social-networks-block">
                                <div class="social-networks-block__text">
                                    <p>Нажмите на соотвествующий значок социальной сети, 
                                        чтобы связать ее с Вашим личным кабинетом</p>
                                </div>
                                <div class="social-networks-block__icons">
                                    <a href="#"><i class="fab fa-facebook-f"></i></a>
                                    <a href="#"><i class="fab fa-twitter"></i></a>
                                    <a href="#"><i class="fab fa-google-plus-g"></i></a>
                                    <a href="#"><i class="fab fa-instagram"></i></a>
                                    <a href="#"><i class="fab fa-vk"></i></a>
                                    <a href="#"><i class="fas fa-at"></i></a>
                                    <a href="#"><i class="fab fa-odnoklassniki"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-2-2">
                        <div class="my-data__mobail-phone">
                            <span class="my-data__title-for-block">Номер мобильного телефона</span>
                            <div class="my-data__mobail-phone-internal mobail-phone-block">
                                <div class="mobail-phone-block__text">
                                    <p>Изменить номер мобильного телефона можно не более 2-х раз в сутки.</p>
                                </div>
                                <div class="mobail-phone-block__tell">
                                    <p>+7 (965) 242-29-99</p>
                                </div>
                            </div>
                            <div class="my-data__buttom-modal-window">
                                <span class="button-phone">Изменить телефон</span>
                            </div> 
                        </div>
                        <div class="my-data__e-mail-address">
                            <span class="my-data__title-for-block">Адрес электронной почты</span>
                            <div class="my-data__e-mail-address-internal e-mail-address-block">
                                <div class="e-mail-address-block__mail">
                                    <a href="#">kristinaa89@bk.ru</a>
                                </div>
                            </div>
                            <div class="my-data__buttom-modal-window">
                                <span class="change-email-address">Изменить адрес электронной почты</span>
                            </div>
                        </div>
                        <div class="my-data__password">
                            <span class="my-data__title-for-block">Пароль</span>
                                <div class="my-data__password-internal password-block">
                                    <div class="password-block__text">
                                        <p>Здесь Вы можете изменить свой пароль для входа в личный кабинет</p>
                                    </div>
                                </div>
                                    <div class="my-data__buttom-modal-window">
                                        <span class="button-change-password">Смена пароля</span>
                                    </div> 
                        </div>
                        <div class="my-data__input-protection">
                            <span class="my-data__title-for-block">Дополнительная защита входа</span>
                                <div class="my-data__input-protection-internal input-protection-block">
                                    <div class="input-protection-block__text">
                                        <p>Данная функция позволяет настроить вход на сайт с подтверждением по СМС</p>
                                    </div>
                                </div>
                                    <div class="my-data__buttom-modal-window">
                                        <span class="button-password">Настройка</span>
                                    </div> 
                        </div>
                            <div class="my-data__shipping-addresses">
                                <span class="my-data__title-for-block">Адреса доставки</span>
                                    <div class="my-data__shipping-addresses-internal shipping-addresses-block">
                                        <div class="shipping-addresses-block__text">
                                            <p>Дымова Лариса Сергеевна, +7 (926) 220-63-86
                                            <button> <i></i> </button>
                                            <button> <i></i> </button></p>
                                        </div>
                                    </div>
                                        <div class="my-data__buttom-modal-window">
                                            <span class="button-yandex-map">Добавить адрес</span>
                                        </div> 
                            </div>
                            <div class="my-data__reqisites">
                                <span class="my-data__title-for-block">Реквизиты</span>
                                    <div class="my-data__reqisites-internal reqisites-block">
                                        <div class="reqisites-block__icons">
                                            <img src="/images/my-data/exclamation.png" alt="">
                                        </div>

                                        <div class="reqisites-block__text">
                                            <p>Уважаемые Клиенты! Обращаем Ваше внимание на 
                                                то, что в связи с ограничениями стандартов
                                                безопасности платежных шлюзов PCI DSS, возникают трудности
                                                с возвратами денежных средств на 
                                                банковские счета, начинающиеся на 302*******.
                                                Ограничения связаны с передачей 
                                                номера карты для совершения транзакции.</p>
                                        </div>
                                    </div>
                                    <div class="my-data__buttom-modal-window">
                                        <span>Добавить реквизиты</span>
                                    </div>
                            </div>
                            <div class="my-data__shipping-addresses">
                                <span class="my-data__title-for-block">Получатели</span>
                                <div class="my-data__shipping-addresses-internal shipping-addresses-block">
                                    <div class="shipping-addresses-block__text">
                                        <p>Дымова Лариса Сергеевна, +7 (926) 220-63-86
                                    <button> <i></i> </button>
                                    <button> <i></i> </button>
                                    </div>
                                </div>
                                <div class="my-data__buttom-modal-window">
                                    <span class="button-recipient">Добавить получателя</span>
                                </div> 
                            </div>
                </div>
            </div>
        </div>
    </div>
</div>